/// <reference path = "../lib/phaser.d.ts"/>


module StickHeroClone {
    // CHANGE: to your game name
    class StickHeroClone extends Phaser.Game {
        constructor(width?:number, height?:number) {
            // Creates the game
            super(width, height, Phaser.AUTO, 'phaser-div');
            this.state.add("Preload", Preload, false);
            this.state.add("Menu", Menu, false);
            this.state.add("Game", Game, false);
            this.state.add("GameOver", GameOver, false);
            this.state.start("Preload");
        }
    }

    window.onload = ()=> {
        // CHANGE: to your game name
        new StickHeroClone(1280, 720);
    }
}