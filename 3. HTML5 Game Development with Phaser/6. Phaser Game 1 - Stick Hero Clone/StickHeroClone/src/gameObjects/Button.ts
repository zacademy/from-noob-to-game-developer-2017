module StickHeroClone{
    export class Button extends Phaser.Sprite{

        text:Phaser.Text;
        constructor(game:Phaser.Game,x:number,y:number, text:string, colorInt:number, colorString:string, hoverColorInt:number, hoverColorString:string){
            super(game,x,y, game.cache.getBitmapData("bmdButton"));
            this.game = game;
            this.anchor.set(0.5);
            this.tint = colorInt;

            this.text = this.game.add.text(this.x,this.y,text,{font:"26px Rammetto One", fill:colorString});
            this.text.anchor.set(0.5);
            this.text.position = this.position;
            this.text.setShadow(0,0,colorString,5);

            this.inputEnabled = true;
            this.events.onInputOver.add(()=>{
               this.tint = hoverColorInt;
               this.text.fill = hoverColorString;
                this.text.setShadow(0,0,hoverColorString,5);
            },this);

            this.events.onInputOut.add(()=>{
                this.tint = colorInt;
                this.text.fill = colorString;
                this.text.setShadow(0,0,colorString,5);
            },this);
            this.game.add.tween(this).to({x:this.game.width/2},1000, Phaser.Easing.Quintic.Out,true);
            this.game.add.existing(this);
        }
    }
}