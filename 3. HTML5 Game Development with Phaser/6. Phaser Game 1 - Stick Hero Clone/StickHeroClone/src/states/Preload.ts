/// <reference path = "../../lib/phaser.d.ts"/>
module StickHeroClone {
    export class Preload extends Phaser.State {
        bmd: Phaser.BitmapData;

        preload() {
            this.createButtonTemplate();
            this.createPlayer();
            this.createPlatform();
            this.createLedge();
        }

        create() {
            this.game.state.start("Menu");
        }

        createButtonTemplate() {
            let totalBorder = GLOW_WIDTH + BORDER_WIDTH;
            this.bmd = this.game.make.bitmapData(this.game.width * 0.15 + 4 * (totalBorder), this.game.height * 0.08 + 4 * (totalBorder), "bmdButton", true);
            this.bmd.ctx.strokeStyle = "#ffffff";
            this.bmd.ctx.shadowColor = "#ffffff";
            this.bmd.ctx.shadowOffsetX = 0;
            this.bmd.ctx.shadowOffsetY = 0;
            this.bmd.ctx.shadowBlur = GLOW_WIDTH;
            this.bmd.ctx.lineWidth = BORDER_WIDTH;

            this.bmd.ctx.lineJoin = "round";
            this.bmd.ctx.strokeRect(totalBorder, totalBorder, this.bmd.width - 2 * totalBorder, this.bmd.height - 2 * totalBorder);
        }

        createPlayer() {
            let totalBorder = BORDER_WIDTH + GLOW_WIDTH;
            let playerRadius = this.game.height * PLAYER_RADIUS_FRAC;
            this.bmd = this.game.make.bitmapData(playerRadius + 6 * totalBorder, playerRadius + 6 * totalBorder, "player", true);
            this.bmd.ctx.strokeStyle = "#ffffff";
            this.bmd.ctx.shadowColor = "#ffffff";
            this.bmd.ctx.shadowOffsetX = 0;
            this.bmd.ctx.shadowOffsetY = 0;
            this.bmd.ctx.shadowBlur = GLOW_WIDTH;
            this.bmd.ctx.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.arc(this.bmd.width / 2, this.bmd.height / 2, playerRadius, 0, 2 * Math.PI);
            this.bmd.ctx.stroke();
        }

        createPlatform() {
            let totalBorder = GLOW_WIDTH + BORDER_WIDTH;
            this.bmd = this.game.make.bitmapData(this.game.width * 0.2 + 4 * (totalBorder), this.game.width * 0.2 + 4 * (totalBorder), "platform", true);
            this.bmd.ctx.strokeStyle = "#ffffff";
            this.bmd.ctx.shadowColor = "#ffffff";
            this.bmd.ctx.shadowOffsetX = 0;
            this.bmd.ctx.shadowOffsetY = 0;
            this.bmd.ctx.shadowBlur = GLOW_WIDTH;
            this.bmd.ctx.lineWidth = BORDER_WIDTH;

            this.bmd.ctx.lineJoin = "round";
            this.bmd.ctx.strokeRect(totalBorder, totalBorder, this.bmd.width - 2 * totalBorder, this.bmd.height - 2 * totalBorder);
        }

        createLedge() {
            this.bmd = this.game.make.bitmapData(4, 4, "ledge", true);
            this.bmd.ctx.fillStyle = "#ffffff";
            this.bmd.ctx.shadowColor = "#ffffff";
            this.bmd.ctx.shadowOffsetX = 0;
            this.bmd.ctx.shadowOffsetY = 0;
            this.bmd.ctx.shadowBlur = 1;
            this.bmd.ctx.fillRect(0, 0, 4, 4);


        }
    }
}