module StickHeroClone {
    export class GameOver extends Phaser.State {
        score: number;
        gameOverText: Phaser.Text;
        tryAgainButton: Phaser.Sprite;
        tryAgainText: Phaser.Text;

        init(score: number) {
            this.score = score;
        }

        create() {
            this.game.input.enabled = true;
            this.gameOverText = this.game.add.text(this.game.width * 0.5,
                this.game.height * 0.3, "GAME OVER!\n SCORE: " + this.score + "\nTRY AGAIN?", {
                    font: "26px Rammetto One",
                    fill: "#ff0000"
                });
            this.gameOverText.anchor.set(0.5, 0.5);
            this.gameOverText.setShadow(0, 0, "#ff0000", 5);
            this.gameOverText.align = "center";

            this.tryAgainButton = new Button(this.game,this.game.width*1.2,this.game.height*0.5,"YES",0x00ff00,"#00ff00",0x00bf00,"#00bf00");
            this.tryAgainButton.events.onInputDown.add(()=>{
                this.game.state.start("Game");
            },this);
            this.tryAgainButton.bringToTop();
        }
    }
}