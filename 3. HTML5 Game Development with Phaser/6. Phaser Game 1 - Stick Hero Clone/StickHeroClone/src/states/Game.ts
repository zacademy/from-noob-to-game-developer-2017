/// <reference path = "../../lib/phaser.d.ts"/>
///<reference path="../Utils.ts"/>

module StickHeroClone {
    export class Game extends Phaser.State {
        player: Phaser.Sprite;
        leftPlatform: Phaser.Sprite;
        rightPlatform: Phaser.Sprite;
        initialPlatformPosition: Phaser.Point;
        ledge: Phaser.Sprite;
        playerRadius: number;
        offset: number;
        score: number;
        scoreText: Phaser.Text;
        emitter: Phaser.Particles.Arcade.Emitter;

        create() {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.playerRadius = PLAYER_RADIUS_FRAC * this.game.height;
            this.offset = (BORDER_WIDTH + GLOW_WIDTH) / 2;
            this.player = this.game.add.sprite(this.game.width * 0.15, this.game.height * 0.7, this.game.cache.getBitmapData("player"));
            this.player.anchor.set(0.5);
            this.player.tint = PURPLE_INT;

            this.initialPlatformPosition = new Phaser.Point(this.player.x + this.playerRadius + this.offset * 2, this.player.y + this.playerRadius - this.offset);

            this.leftPlatform = this.game.add.sprite(0, 0, this.game.cache.getBitmapData("platform"));
            this.leftPlatform.tint = AQUA_INT;
            this.leftPlatform.anchor.set(1, 0);
            this.leftPlatform.position.x = this.initialPlatformPosition.x;
            this.leftPlatform.position.y = this.initialPlatformPosition.y;
            this.player.bringToTop();

            this.rightPlatform = this.game.add.sprite(0, 0, this.game.cache.getBitmapData("platform"));
            this.rightPlatform.tint = AQUA_INT;
            this.rightPlatform.anchor.set(1, 0);
            this.rightPlatform.position.x = this.getRandomX();
            this.rightPlatform.position.y = this.initialPlatformPosition.y;

            this.ledge = this.game.add.sprite(400, 400, this.game.cache.getBitmapData("ledge"));
            this.ledge.anchor.set(0.5, 0);
            this.ledge.position.set(this.playerRadius, this.playerRadius * 1.1);
            this.ledge.tint = 0x0000ff;
            this.player.addChild(this.ledge);
            this.ledge.bringToTop();

            this.createEmitter();
            this.score = 0;
            this.scoreText = this.game.add.text(this.game.width/2, this.game.height*0.2,"SCORE: "+this.score,{font:"26px Rammetto One", fill:"#FF6F00"});
            this.scoreText.anchor.set(0.5);
            this.scoreText.setShadow(0, 0, "#FF6F00", 5);

            this.game.add.tween(this.game.world)
                .from({alpha: 0}, 500, Phaser.Easing.Default, true);
            this.game.input.onUp.add(() => {
                this.startRotation();
                this.game.input.enabled = false;
            }, this);

            this.game.input.onDown.add(() => {
                this.ledge.alpha = 1;
            });
        }

        update() {
            if (this.game.input.activePointer.isDown) {
                this.ledge.scale.y -= LEDGE_SPEED;
            }
        }

        render() {

        }

        getRandomX() {
            return this.leftPlatform.width * 2 + Math.random() * this.leftPlatform.width * 1.5;
        }

        startRotation() {
            let rotationTween = this.game.add.tween(this.ledge)
                .to({rotation: Math.PI / 2}, 500, Phaser.Easing.Quartic.In, true);
            rotationTween.onComplete.add(() => {
                this.checkSuccessful();
            }, this)
        }

        checkSuccessful() {
            if (-this.ledge.height > Math.abs(this.leftPlatform.x - this.rightPlatform.x) - this.leftPlatform.width
                && -this.ledge.height < Math.abs(this.leftPlatform.x - this.rightPlatform.x)) {
                this.successLogic();
            }
            else {
                this.game.time.events.add(800, () => {
                    this.game.state.start("GameOver", true, false, this.score);
                }, this);
                this.ledge.tint = 0xff0000;
            }
        }

        successLogic() {
            this.score++;
            this.scoreText.text = "SCORE: " + this.score;
            this.ledge.tint = 0x00ff00;
            this.emitter.position.set(this.ledge.world.x - this.ledge.height, this.ledge.world.y);
            this.emitter.start(true, 3000, 0, 40);
            let playerTween = this.game.add.tween(this.player);

            this.game.time.events.add(500, () => {
                this.ledge.alpha = 0;
                this.ledge.rotation = 0;
                this.ledge.tint = 0x0000ff;
                this.ledge.scale.set(1, 1);
                // Move player to right platform
                playerTween.to({x: this.rightPlatform.x - this.playerRadius}, 600, Phaser.Easing.Quintic.In, true);
                playerTween.onComplete.add(() => {
                    // return player to initial position, right platform to left platform, left platform to -500


                    let leftPlatformPosX = this.leftPlatform.x;
                    this.game.add.tween(this.leftPlatform).to({x: -500}, 500, Phaser.Easing.Default, true, 500);
                    this.game.add.tween(this.rightPlatform).to({x: leftPlatformPosX}, 500, Phaser.Easing.Default, true, 500);
                    let returnToInitialTween = this.game.add.tween(this.player);
                    returnToInitialTween.to({x: this.game.width * 0.15}, 500, Phaser.Easing.Default, true, 500);
                    returnToInitialTween.onComplete.add(() => {
                        this.leftPlatform.alpha = 0;
                        this.leftPlatform.x = this.getRandomX();
                        let alphaTween = this.game.add.tween(this.leftPlatform);
                        alphaTween.to({alpha: 1}, 300, Phaser.Easing.Default, true);
                        alphaTween.onComplete.add(() => {
                            let swap = this.rightPlatform;
                            this.rightPlatform = this.leftPlatform;
                            this.leftPlatform = swap;
                            this.game.input.enabled = true;
                        },this);
                    },this);
                }, this)
            }, this)
        }

        createEmitter() {
            this.emitter = this.game.add.emitter(0, 0, 30);
            this.emitter.gravity = 700;
            this.emitter.makeParticles(this.game.cache.getBitmapData("ledge"));
            this.emitter.children.forEach((p: Phaser.Sprite) => {
                p.tint = 0xFFC100;
            });
        }
    }
}