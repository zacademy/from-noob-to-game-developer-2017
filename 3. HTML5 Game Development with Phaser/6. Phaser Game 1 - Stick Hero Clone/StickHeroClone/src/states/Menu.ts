/// <reference path = "../../lib/phaser.d.ts"/>
///<reference path="../Utils.ts"/>
///<reference path="../gameObjects/Button.ts"/>

module StickHeroClone {

    export class Menu extends Phaser.State {

        startButton: Phaser.Sprite;
        exitButton: Phaser.Sprite;

        create() {
            this.game.stage.backgroundColor = "#222222";
            this.startButton = new Button(this.game,-this.game.width*0.1,this.game.height*0.3, "START", PURPLE_INT,PURPLE_STR,PURPLE_DARK_INT,PURPLE_DARK_STR);
            this.exitButton = new Button(this.game,this.game.width*1.1,this.game.height*0.6, "EXIT", AQUA_INT,AQUA_STR,AQUA_DARK_INT,AQUA_DARK_STR);

            this.startButton.events.onInputDown.add(()=>{
                let startTween = this.game.add.tween(this.startButton).to({x:-this.game.width*0.1}, 1000, Phaser.Easing.Quintic.Out,true);
                this.game.add.tween(this.exitButton).to({x:this.game.width*1.1}, 1000, Phaser.Easing.Quintic.Out,true);
                startTween.onComplete.add(()=>{
                    this.game.state.start("Game");
                }, this);

            },this);

            this.exitButton.events.onInputDown.add(()=>{
               this.game.destroy();
            });
        }
    }
}