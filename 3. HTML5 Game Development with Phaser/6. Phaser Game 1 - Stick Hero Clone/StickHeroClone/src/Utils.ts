module StickHeroClone{
    export const BORDER_WIDTH = 5;
    export const GLOW_WIDTH = 15;
    export const PLAYER_RADIUS_FRAC = 0.1;
    export const PURPLE_STR = "#FF30D7";
    export const PURPLE_INT = 0xFF30D7;
    export const PURPLE_DARK_STR = "#b72597";
    export const PURPLE_DARK_INT = 0xb72597;
    export const AQUA_STR = "#29EDFF";
    export const AQUA_INT = 0x29EDFF;
    export const AQUA_DARK_STR = "#22B3C5";
    export const AQUA_DARK_INT = 0x22B3C5;
    export const LEDGE_SPEED = 2;
}