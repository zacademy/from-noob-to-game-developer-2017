/// <reference path = "definitions/phaser.d.ts"/>

module FlappyBird {
    export class FlappyBird extends Phaser.Game {
        constructor(width?:number, height?:number) {
            // Creates the game
            super(width, height, Phaser.AUTO, 'phaser-div');

            this.state.add("Preload", Preload, false);
            this.state.add("Game", Game, false);
            this.state.start("Preload");
        }
    }

    window.onload = ()=> {
        // CHANGE: to your game name
        new FlappyBird(576, 1024);
    }
}