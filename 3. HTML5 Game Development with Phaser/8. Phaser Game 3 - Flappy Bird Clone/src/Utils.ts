module FlappyBird {
    export const PRELOADER_BAR_EMPTY_WIDTH: number = 9;
    export const PRELOADER_BAR_FILL_WIDTH: number = 5;

    export const BIRD_FRAME_WIDTH: number = 34;
    export const BIRD_FRAME_HEIGHT: number = 24;
    export const BIRD_GRAVITY: number = 1000;
    export const BIRD_VELOCITY_Y: number = 500;
    export const BIRD_POSITION_X: number = 100;

    export const LEVEL_FOREGROUND_SPEED: number = 5;
    export const LEVEL_FOREGROUND_POSITION_Y: number = 0.85;
    export const LEVEL_FOREGROUND_WIDTH: number = 1.2;

    export const OBSTACLE_HOLE_SIZE: number = 300;
    export const OBSTACLE_HOLE_MINIMUM_Y: number = 100;
    export const OBSTACLE_HOLE_MAX_Y: number = 400;
    export const OBSTACLE_GAP: number = 0.7;
    export const OBSTACLE_STARTING_POSITION = 1.1;

    export const ENVIRONMENT_SPEED: number = 300;
}