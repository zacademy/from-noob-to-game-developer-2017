///<reference path="../definitions/phaser.d.ts"/>
///<reference path="../Utils.ts"/>

module FlappyBird {
    export class Preload extends Phaser.State {
        private preloaderGraphics: Phaser.Graphics;
        private percentDoneLabel: Phaser.Text;

        public create(): void {
            this.game.stage.disableVisibilityChange = true;
            this.game.stage.backgroundColor = 0x123456;

            this.preloaderGraphics = this.game.add.graphics(this.game.world.centerX, this.game.world.centerY);
            this.preloaderGraphics.lineStyle(PRELOADER_BAR_EMPTY_WIDTH, 0xffffff, 0.2);
            this.preloaderGraphics.arc(0, 0, 80, Phaser.Math.degToRad(0), Phaser.Math.degToRad(360), false);
            this.preloaderGraphics.lineStyle(PRELOADER_BAR_FILL_WIDTH, 0xffffff);
            this.preloaderGraphics.arc(0, 0, 80, Phaser.Math.degToRad(-90), Phaser.Math.degToRad(-90), false);

            this.percentDoneLabel = new Phaser.Text(this.game, 0, 0, "0%", {
                font: "50px OpenSans",
                fill: "#ffffff",
                align: "center"
            });
            this.percentDoneLabel.x = this.game.world.centerX;
            this.percentDoneLabel.y = this.game.world.centerY;
            this.percentDoneLabel.anchor.set(0.5, 0.5);
            this.add.existing(this.percentDoneLabel);

            this.load.spritesheet("blueBird", "src/assets/birds/blue_bird.png", BIRD_FRAME_WIDTH, BIRD_FRAME_HEIGHT);
            this.load.spritesheet("redBird", "src/assets/birds/red_bird.png", BIRD_FRAME_WIDTH, BIRD_FRAME_HEIGHT);
            this.load.spritesheet("yellowBird", "src/assets/birds/yellow_bird.png", BIRD_FRAME_WIDTH, BIRD_FRAME_HEIGHT);

            this.load.image("background-day", "src/assets/background-day.png");
            this.load.image("background-night", "src/assets/background-night.png");
            this.load.image("base", "src/assets/base.png");
            this.load.image("gameover", "src/assets/gameover.png");
            this.load.image("pipe-green", "src/assets/pipe-green.png");
            this.load.image("pipe-red", "src/assets/pipe-red.png");

            for (let i: number = 1; i <= 24; i++) {
                let coinIndex: string = ('0000' + i ).slice(-4);

                this.load.image("coinBr" + i, "src/assets/loading-demo/particles/CoinBrNew_" + coinIndex + ".png");
                this.load.image("coinG" + i, "src/assets/loading-demo/particles/CoinGNew_" + coinIndex + ".png");
                this.load.image("coinLg" + i, "src/assets/loading-demo/particles/CoinLgNew_" + coinIndex + ".png");
            }

            for (let i: number = 1; i <= 50; i++) {
                let symIndex: string = ('000' + i ).slice(-3);

                this.load.image("sym8_anim_" + i, "src/assets/loading-demo/symbol_animations/sym_bonus_seq/sym8_anim_" + symIndex + ".png");
                this.load.image("sym9_anim_" + i, "src/assets/loading-demo/symbol_animations/sym_scatter_win_seq/sym10_anim_" + symIndex + ".png");
                this.load.image("sym10_anim_" + i, "src/assets/loading-demo/symbol_animations/sym_wild_seq/sym9_anim_" + symIndex + ".png");

            }

            for (let i: number = 0; i <= 12; i++) {
                this.load.image("sym" + i, "src/assets/loading-demo/symsHD/sym" + i + ".png");
                this.load.image("symBlur" + i, "src/assets/loading-demo/symsHD/sym" + i + "_blur.png");
            }

            // every file loaded
            this.load.onFileComplete.add(this.onFileComplete, this);

            // all files loaded
            this.load.onLoadComplete.add(this.onLoadComplete, this);

            this.load.start();
        }

        private onFileComplete(progress: any, cacheKey: any, success: any, totalLoaded: any, totalFiles: any) {
            this.preloaderGraphics.clear();
            this.preloaderGraphics.lineStyle(PRELOADER_BAR_EMPTY_WIDTH, 0xffffff, 0.2);
            this.preloaderGraphics.arc(0, 0, 80, Phaser.Math.degToRad(0), Phaser.Math.degToRad(360), false);
            this.preloaderGraphics.lineStyle(PRELOADER_BAR_FILL_WIDTH, 0xffffff);
            this.preloaderGraphics.arc(0, 0, 80, Phaser.Math.degToRad(-90), Phaser.Math.degToRad(Math.round(totalLoaded / totalFiles * 360 - 90)), false);

            this.percentDoneLabel.setText(progress + "%");
        }

        private onLoadComplete() {
            let tween: Phaser.Tween = this.game.add.tween(this.preloaderGraphics);
            tween.to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 0);
            tween.onComplete.add(this.continueToGame, this);

            let tweenText: Phaser.Tween = this.game.add.tween(this.percentDoneLabel);
            tweenText.to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 0);
        }

        private continueToGame() {
            this.game.state.start('Game');
        }

        shutdown() {
            if (this.preloaderGraphics != null) {
                this.preloaderGraphics.destroy(true);
                this.preloaderGraphics = null;
            }

            if (this.percentDoneLabel != null) {
                this.percentDoneLabel.destroy(true);
                this.percentDoneLabel = null;
            }
        }
    }
}