///<reference path="../definitions/phaser.d.ts"/>
///<reference path="../gameObjects/Bird.ts"/>

module FlappyBird {
    export class Game extends Phaser.State {
        private bird: Bird;
        private level: Level;

        private flyKey: Phaser.Key;
        private hasGameStarted: boolean;

        private scoreText: Phaser.Text;
        private score: number;

        private pauseKey: Phaser.Key;

        public create(): void {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.hasGameStarted = false;

            this.level = new Level(this.game);
            this.add.existing(this.level);

            for (let i:number = 0; i < this.level.pipes.length; i++) {
                this.level.pipes[i].onReset.add(this.updateScore, this);
            }

            this.bird = new Bird(this.game);
            this.add.existing(this.bird);

            this.score = 0;
            this.scoreText = new Phaser.Text(this.game, 10, 20, this.score.toString(), {
                font: "50px",
                fill: "#ffffff"
            });
            this.add.existing(this.scoreText);

            // Show input handler
            this.flyKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            this.flyKey.onDown.addOnce(this.startGame, this);
            this.flyKey.onDown.add(this.birdFlap, this);

            this.pauseKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
            this.pauseKey.onDown.add(()=>{
                this.game.paused = !this.game.paused;
            })
        }

        public update(): void {
            if (this.hasGameStarted) {
                this.bird.updateBird();

                this.game.physics.arcade.collide(this.bird, this.level.pipes, this.gameOver, null, this);

                if (this.bird.y < 0 || this.bird.y > this.game.height * LEVEL_FOREGROUND_POSITION_Y) {
                    this.gameOver();
                }
            }
        }

        private updateScore(): void {
            this.score++;
            this.scoreText.text = this.score.toString();
        }

        private birdFlap(): void {
            this.bird.flap();
            // ...
            // ...
            // ...
            // ...
        }

        private startGame(): void {
            this.hasGameStarted = true;
            this.bird.start();
            this.level.start();
        }

        private gameOver(): void {
            this.game.state.start("Game", true, false);
        }

        public shutdown(): void {

        }
    }
}