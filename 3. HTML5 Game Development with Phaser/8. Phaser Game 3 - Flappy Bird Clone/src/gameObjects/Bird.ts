module FlappyBird {
    export class Bird extends Phaser.Sprite {
        private flapTween: Phaser.Tween;

        constructor(game: Phaser.Game) {
            super(game, 0, 0);

            this.loadTexture("blueBird");
            this.anchor.set(0.5);
            this.scale.set(2);
            this.position.set(BIRD_POSITION_X, this.game.height * 0.5);

            this.animations.add("fly", null, 12, true);
            this.animations.play("fly");

            this.flapTween = this.game.add.tween(this);
            this.flapTween.to({angle: -20}, 100, Phaser.Easing.Linear.None);
            // this.flapTween.start();
        }

        public updateBird(): void {
            if (this.angle < 20) {
                this.angle++;
            }
        }

        public start(): void {
            this.game.physics.enable(this, Phaser.Physics.ARCADE);
            this.body.gravity.y = BIRD_GRAVITY;
        }

        public flap(): void {
            this.body.velocity.y = -BIRD_VELOCITY_Y;
            this.flapTween.start();
        }
    }
}