///<reference path="../definitions/phaser.d.ts"/>
///<reference path="../Utils.ts"/>

module FlappyBird {
    export class Obstacle extends Phaser.Group {
        public onReset: Phaser.Signal;

        private topPipe: Phaser.Sprite;
        private bottomPipe: Phaser.Sprite;

        private startingPosition: number;
        private endingPosition: number;

        constructor(game: Phaser.Game, startingPosition: number, endingPosition: number) {
            super(game, null);
            this.onReset = new Phaser.Signal();

            this.startingPosition = startingPosition;
            this.endingPosition = endingPosition;

            this.topPipe = new Phaser.Sprite(this.game, 0, startingPosition, "pipe-green");
            this.topPipe.scale.set(2, -2);
            this.topPipe.anchor.set(0.5, 0);
            this.add(this.topPipe);

            this.bottomPipe = new Phaser.Sprite(this.game, 0, endingPosition, "pipe-green");
            this.bottomPipe.scale.set(2);
            this.bottomPipe.anchor.set(0.5, 0);
            this.add(this.bottomPipe);

            this.topPipe.checkWorldBounds = true;
            this.topPipe.events.onEnterBounds.addOnce(() => {
                this.topPipe.events.onOutOfBounds.add(this.reset, this);
            });

        }

        public generateHole(): void {
            let holePosition: number = OBSTACLE_HOLE_MINIMUM_Y +
                Math.round(Math.random() * (this.endingPosition - this.startingPosition - OBSTACLE_HOLE_MAX_Y));

            this.topPipe.position.set(0, holePosition);
            this.topPipe.body.x = this.topPipe.position.x;
            this.topPipe.body.y = this.topPipe.position.y;

            this.bottomPipe.position.set(0, holePosition + OBSTACLE_HOLE_SIZE);
            this.bottomPipe.body.x = 0;
            this.bottomPipe.body.y = holePosition + OBSTACLE_HOLE_SIZE;
        }

        public start(): void {
            this.game.physics.enable(this.topPipe, Phaser.Physics.ARCADE);
            this.topPipe.body.velocity.x = -ENVIRONMENT_SPEED;

            this.game.physics.enable(this.bottomPipe, Phaser.Physics.ARCADE);
            this.bottomPipe.body.velocity.x = -ENVIRONMENT_SPEED;

            this.generateHole();
        }

        public reset(): void {
            this.x = this.game.width * (OBSTACLE_STARTING_POSITION + OBSTACLE_GAP) + this.topPipe.width;

            this.onReset.dispatch();

            this.generateHole();
        }
    }
}