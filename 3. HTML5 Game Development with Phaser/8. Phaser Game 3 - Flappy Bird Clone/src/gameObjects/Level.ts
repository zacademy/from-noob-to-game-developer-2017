///<reference path="../definitions/phaser.d.ts"/>
///<reference path="../Utils.ts"/>
///<reference path="Obstacle.ts"/>

module FlappyBird {
    export class Level extends Phaser.Group {
        private background: Phaser.Sprite;
        private foregrounds: Array<Phaser.Sprite>;
        private obstacles: Array<Obstacle>;

        constructor(game: Phaser.Game) {
            super(game, null);

            this.background = new Phaser.Sprite(this.game, 0, 0, "background-day");
            this.background.width = this.game.width;
            this.background.height = this.game.height;
            this.add(this.background);

            this.obstacles = [];
            for (let i: number = 0; i < 3; i++) {
                let obstacle: Obstacle = new Obstacle(this.game, 0, this.game.height * LEVEL_FOREGROUND_POSITION_Y);
                obstacle.position.set(this.game.width * OBSTACLE_STARTING_POSITION
                    + i * this.game.width * OBSTACLE_GAP, 0);
                this.add(obstacle);
                this.obstacles.push(obstacle);
            }

            this.foregrounds = [];
            for (let i: number = 0; i < 2; i++) {
                let foreground: Phaser.Sprite = new Phaser.Sprite(this.game, i * (this.game.width * LEVEL_FOREGROUND_WIDTH),
                    this.game.height * LEVEL_FOREGROUND_POSITION_Y, "base");
                foreground.width = this.game.width * LEVEL_FOREGROUND_WIDTH;
                foreground.height = this.game.height - foreground.position.y;
                this.game.physics.enable(foreground, Phaser.Physics.ARCADE);
                foreground.body.velocity.x = -ENVIRONMENT_SPEED;
                this.add(foreground);
                this.foregrounds.push(foreground);
            }
        }

        public get pipes(): Array<Obstacle> {
            return this.obstacles;
        }

        public update(): void {
            for (let i: number = 0; i < this.foregrounds.length; i++) {
                if (this.foregrounds[i].position.x + this.foregrounds[i].width < 0) {
                    this.foregrounds[i].position.x = (this.game.width * LEVEL_FOREGROUND_WIDTH) - LEVEL_FOREGROUND_SPEED;
                }

                // this.foregrounds[i].position.x -= LEVEL_FOREGROUND_SPEED;
            }
        }

        public start(): void {
            for (let i:number = 0; i < this.obstacles.length; i++) {
                this.obstacles[i].start();
            }
        }
    }
}