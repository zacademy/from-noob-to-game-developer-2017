///<reference path = "Graph.ts"/>.

var graph = new SimpleGraph.Graph<number>();

graph.addVertexNow(0);
graph.addVertexNow(1);
graph.addVertexNow(2);
graph.addVertexNow(3);

graph.addUndirectedEdge(0,1);
graph.addUndirectedEdge(0,2);
graph.addUndirectedEdge(0,3);
graph.addUndirectedEdge(1,2);

console.log(graph);
