///<reference path="Vertex.ts"/>
module SimpleGraph{
 export class Graph<T> {
    vertices:Vertex<T>[];

    constructor(givenSetOfVertices?:Vertex<T>[]) {
            this.vertices = givenSetOfVertices || [];
        }

     addVertexNow(value:T) {
         var vertex = new Vertex(value);

        this.vertices.forEach((v) => {
            if (v.value === vertex.value) {
                throw new Error("You are not allowed to have vertices with same values")
            }
        });

        this.vertices.push(vertex);
    }

    addDirectedEdge(from:T, to:T) {
        var vFrom = this.vertices[this.findByValue(from)];
        var vTo = this.vertices[this.findByValue(to)];
        vFrom.neighbours.push(vTo);
    }

    addUndirectedEdge(from:T, to:T) {
        this.addDirectedEdge(from, to);
        this.addDirectedEdge(to, from);
    }

    findByValue(value:T) {
        for (var i = 0; i < this.vertices.length; i++) {
            if (this.vertices[i].value === value) {
                return i;
            }
        }
        return null;
    }
}

}