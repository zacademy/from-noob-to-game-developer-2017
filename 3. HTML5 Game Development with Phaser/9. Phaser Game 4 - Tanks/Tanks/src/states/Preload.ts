/**
 * Created by Academy on 4/12/2018.
 */

module Main{

    export class Preload extends Phaser.State{

        preload(){
            this.game.load.image(GROUND_IMAGE_KEY, '../src/assets/ground.png');
            this.game.load.atlasJSONArray(TANK_IMAGE_KEY, '../src/assets/tank.png', '../src/assets/tank.json');
            this.game.load.image(TRACKS_IMAGE_KEY, '../src/assets/tracks.png');
            this.game.load.image(SHELL_IMAGE_KEY, '../src/assets/shell.png');
            this.game.load.atlasJSONArray(EXPLOSION_IMAGE_KEY, '../src/assets/explosion.png',
                '../src/assets/explosion.json');
        }

        create(){
            this.game.state.start(GAME_STATE_KEY);
        }
    }
}