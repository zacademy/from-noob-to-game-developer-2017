/**
 * Created by Academy on 4/12/2018.
 */

module Main {

    export class Game extends Phaser.State {

        private ground: Phaser.TileSprite;
        private player: PlayerTank;
        private playerSpeed: number;
        private wKey: Phaser.Key;
        private aKey: Phaser.Key;
        private sKey: Phaser.Key;
        private dKey: Phaser.Key;
        private enemyTanks: Array<EnemyTank>;
        private drawing: Drawing;
        private score: number;
        private gameOver: boolean;

        create() {
            this.game.world.setBounds(-this.game.width, -this.game.height, this.game.width * 3,
                this.game.height * 3);

            this.ground = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, GROUND_IMAGE_KEY);
            this.ground.fixedToCamera = true;
            this.drawing = new Drawing(this.game);

            this.player = new PlayerTank(this.game, this.game.width * 0.5, this.game.height * 0.5,
                TANK_IMAGE_KEY, [0, 1, 2], WEAPON_FRAME_NAME, this.game.width * 0.06, PLAYER_STARTING_HP,
                PLAYER_STARTING_HP, EXPLOSION_IMAGE_KEY);
            this.game.camera.follow(this.player);
            this.playerSpeed = 0;
            this.score = 0;

            this.generateEnemies();
            this.attachControls();
        }

        render() {
            this.game.debug.text("Score : " + this.score.toString(), this.game.width * 0.05, this.game.height * 0.05,
                "#fff", "32px sans-serif");
        }

        update() {
            this.game.physics.arcade.collide(this.player, this.enemyTanks);
            this.game.physics.arcade.overlap(this.player.shellsCollection, this.enemyTanks,
                (shell: Shell, enemy: EnemyTank) => {
                    this.score += PLAYER_SCORE_HIT_INCREMENT;
                    this.player.removeShell(shell);
                    this.emitSparks(shell.x, shell.y, shell.width * 0.3);
                    shell.destroy();
                    let enemyDead = enemy.takeDamage(PLAYER_DAMAGE);

                    if (enemyDead) {
                        this.score += PLAYER_SCORE_KILL_INCREMENT;
                        this.enemyTanks.splice(this.enemyTanks.indexOf(enemy), 1);
                    }
                }, null, this);

            for (let i = 0; i < this.enemyTanks.length; i++) {
                this.game.physics.arcade.collide(this.enemyTanks[i], this.player);
                this.game.physics.arcade.overlap(this.enemyTanks[i].shellsCollection, this.player,
                    (shell: Shell, player: PlayerTank) => {
                        shell.destroy();
                        this.enemyTanks[i].removeShell(shell);
                        this.score -= PLAYER_HIT_PENALTY;
                        this.gameOver = player.takeDamage(ENEMY_DAMAGE);
                    }, null, this);
            }

            if (this.gameOver) {
                this.game.state.start(PRELOAD_STATE_KEY, true, false);
            }

            if (this.sKey.isDown) {
                if (this.playerSpeed === 0) {
                    this.player.emitSmoke();
                    this.player.createTracks(this.player.x, this.player.y, this.player.width,
                        this.player.height, this.player.angle, TRACKS_IMAGE_KEY);
                }

                this.playerSpeed -= MOVE_ACCELERATION_STEP;
            }

            if (this.wKey.isDown) {
                if (this.playerSpeed === 0) {
                    this.player.emitSmoke();
                    this.player.createTracks(this.player.x, this.player.y, this.player.width,
                        this.player.height, this.player.angle, TRACKS_IMAGE_KEY);
                }

                this.playerSpeed += MOVE_ACCELERATION_STEP;
            }

            if (this.sKey.isUp && this.wKey.isUp && this.playerSpeed !== 0) {
                if (Math.abs(this.playerSpeed - MOVE_DECELERATION_STEP) >= MOVE_DECELERATION_STEP) {
                    this.playerSpeed += this.playerSpeed > 0 ? -MOVE_DECELERATION_STEP : MOVE_DECELERATION_STEP;
                }
                else {
                    this.playerSpeed = 0;
                }
            }

            if (this.playerSpeed < -MAX_MOVEMENT_SPEED) {
                this.playerSpeed = -MAX_MOVEMENT_SPEED;
            }
            else if (this.playerSpeed > MAX_MOVEMENT_SPEED) {
                this.playerSpeed = MAX_MOVEMENT_SPEED;
            }

            let rotating: boolean = false;

            if (this.aKey.isDown) {
                rotating = !rotating;
                this.player.angle -= BODY_ROTATION_STEP;
            }

            if (this.dKey.isDown) {
                rotating = !rotating;
                this.player.angle += BODY_ROTATION_STEP;
            }

            if (rotating && this.playerSpeed === 0) {
                this.player.emitSmoke();
            }

            if (this.playerSpeed !== 0 || rotating) {
                this.player.startAnimation();
            }
            else {
                this.player.stopAnimation();
            }

            this.game.physics.arcade.velocityFromRotation(this.player.rotation - Math.PI * 0.5, this.playerSpeed,
                this.player.body.velocity);
            this.player.turret.rotation = this.game.physics.arcade.angleToPointer(this.player.turret,
                    this.game.input.activePointer) + Math.PI * 0.5;

            this.ground.tilePosition.x = -this.game.camera.x;
            this.ground.tilePosition.y = -this.game.camera.y;

            if (this.game.input.activePointer.isDown && this.player.canFire()) {
                this.player.disableFireAbility();
                this.player.fire(new Phaser.Point(this.game.input.activePointer.worldX,
                    this.game.input.activePointer.worldY), PLAYER_BULLET_VELOCITY, PLAYER_BULLET_MOMENTUM_VELOCITY);
            }
            else if (this.game.input.activePointer.isUp) {
                this.player.resetFireAbility();
            }

            this.player.update();
            this.enemyTanks.forEach((tank) => {
                tank.update();
                tank.updateTargetedComponents(this.player);
            });
        }

        shutdown() {
            console.log('shutting down');
            this.ground.destroy();
            this.ground = null;
            this.player.destroy();
            this.playerSpeed = null;
            this.wKey = null;
            this.aKey = null;
            this.sKey = null;
            this.dKey = null;
            this.enemyTanks.forEach((tank) => {
                tank.destroy();
                tank = null;
            });
            this.enemyTanks = null;
            this.drawing.destroy();
            this.gameOver = null;

            super.shutdown();
        }

        private attachControls() {
            this.wKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
            this.aKey = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
            this.sKey = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
            this.dKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        }

        private checkTankExistsAtPosition(tanks: Array<Tank>, x: number, y: number): boolean {
            for (let i = 0; i < tanks.length; i++) {
                if (Math.abs(x - tanks[i].x) < tanks[i].width &&
                    Math.abs(y - tanks[i].y) < tanks[i].height) {
                    return true;
                }
            }

            return false;
        }

        private generateEnemies(): void {
            let enemyWidth = this.game.width * 0.05;

            this.enemyTanks = [];

            for (let i = 0; i < ENEMIES_COUNT; i++) {
                let x: number;
                let y: number;

                do {
                    x = this.game.rnd.integerInRange(this.game.world.bounds.left + enemyWidth * 1.5,
                        this.game.world.bounds.right - enemyWidth * 1.5);
                    y = this.game.rnd.integerInRange(this.game.world.bounds.top + enemyWidth * 3,
                        this.game.world.bounds.bottom - enemyWidth * 3);
                }
                while (this.checkTankExistsAtPosition(this.enemyTanks, x, y) &&
                this.checkTankExistsAtPosition([this.player], x, y));

                this.enemyTanks.push(new EnemyTank(this.game, x, y, TANK_IMAGE_KEY, [0, 1, 2], WEAPON_FRAME_NAME,
                    enemyWidth, ENEMY_STARTING_HP, ENEMY_STARTING_HP, EXPLOSION_IMAGE_KEY, ENEMY_MIN_MOVEMENT_SPEED,
                    ENEMY_MAX_MOVEMENT_SPEED, ENEMY_MOVEMENT_CHANGE_MIN_DELAY, ENEMY_MOVEMENT_CHANGE_MAX_DELAY));
            }
        }

        private emitSparks(x: number, y: number, emitterWidth: number): void {
            let sparksEmitter = this.game.add.emitter(x, y);
            sparksEmitter.width = emitterWidth;
            sparksEmitter.particleAnchor.set(0.5);
            sparksEmitter.makeParticles(this.game.cache.getBitmapData(SPARK_BMD_KEY));
            sparksEmitter.setAlpha(0.3, 0.7, 30);
            sparksEmitter.minParticleScale = 0.1;
            sparksEmitter.maxParticleScale = 0.1;
            sparksEmitter.start(false, 400, 3, 20);
            this.game.time.events.add(600, () => {
                sparksEmitter.destroy();
                sparksEmitter = null;
            })
        }
    }
}