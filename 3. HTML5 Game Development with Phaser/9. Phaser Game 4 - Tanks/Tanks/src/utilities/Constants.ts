/**
 * Created by Academy on 4/12/2018.
 */

module Main {

    export const PRELOAD_STATE_KEY: string = 'Preload';
    export const GAME_STATE_KEY: string = 'Game';

    export const GROUND_IMAGE_KEY: string = 'Ground';
    export const TANK_IMAGE_KEY: string = 'Tank';

    export const MOVE_ANIMATION_KEY: string = 'Move';
    export const TRACKS_IMAGE_KEY: string = 'Tracks';
    export const SHELL_IMAGE_KEY: string = 'Shell';
    export const WEAPON_FRAME_NAME: string = 'Weapon';
    export const EXPLOSION_IMAGE_KEY: string = 'Explosion';
    export const EXPLOSION_ANIMATION_KEY: string = 'Explosion';
    export const SMOKE_BMD_KEY: string = 'Smoke';
    export const SPARK_BMD_KEY: string = 'Spark';

    export const MOVE_ACCELERATION_STEP: number = 10;
    export const MOVE_DECELERATION_STEP: number = 5;
    export const MAX_MOVEMENT_SPEED: number = 500;
    export const BODY_ROTATION_STEP: number = 5;
    export const PLAYER_BULLET_VELOCITY: number = 6;
    export const PLAYER_BULLET_MOMENTUM_VELOCITY: number = 2;
    export const ENEMY_BULLET_VELOCITY: number = 3;
    export const ENEMY_BULLET_MOMENTUM_VELOCITY: number = 1;
    export const ENEMY_MIN_MOVEMENT_SPEED: number = 1;
    export const ENEMY_MAX_MOVEMENT_SPEED: number = 3;

    export const ENEMY_FIRE_RELOAD_DELAY: number = 3000;
    export const ENEMY_FIRE_INITIAL_DELAY: number = 2000;

    export const PLAYER_SCORE_HIT_INCREMENT: number = 1;
    export const PLAYER_SCORE_KILL_INCREMENT: number = 5;
    export const PLAYER_HIT_PENALTY: number = 5;

    export const ENEMY_MOVEMENT_CHANGE_MIN_DELAY: number = 2000;
    export const ENEMY_MOVEMENT_CHANGE_MAX_DELAY: number = 5000;

    export const PLAYER_STARTING_HP: number = 20;
    export const ENEMY_STARTING_HP: number = 30;

    export const ENEMIES_COUNT: number = 20;

    export const PLAYER_DAMAGE: number = 10;
    export const ENEMY_DAMAGE: number = 3;
}