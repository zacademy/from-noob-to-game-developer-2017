module Main {

    export class Drawing {

        private game: Phaser.Game;
        private smokeBMD: Phaser.BitmapData;
        private sparkBMD: Phaser.BitmapData;

        constructor(game: Phaser.Game) {
            this.game = game;
            this.smokeBMD = this.drawCircle(this.game.width * 0.04, this.game.width * 0.04, SMOKE_BMD_KEY, "#000000");
            this.sparkBMD = this.drawCircle(this.game.width * 0.04, this.game.width * 0.04, SPARK_BMD_KEY, "#000000");
        }

        destroy() {
            this.smokeBMD.destroy();
            this.smokeBMD = null;
            this.sparkBMD.destroy();
            this.sparkBMD = null;
        }

        private drawCircle(width: number, height: number, key: string, color: string): Phaser.BitmapData {
            let bmd = this.game.make.bitmapData(width, height, key, true);
            bmd.ctx.fillStyle = color;
            bmd.ctx.arc(bmd.width * 0.5, bmd.height * 0.5, bmd.width * 0.45,
                0, 360);
            bmd.ctx.fill();

            return bmd;
        }
    }
}
