///<reference path="Tank.ts"/>
module Main {

    export class PlayerTank extends Tank {

        private smokeEmitter: Phaser.Particles.Arcade.Emitter;
        private tracksCollection: Array<Phaser.Sprite>;

        constructor(game: Phaser.Game, x: number, y: number, imageKey: string, bodyFrames: Array<number>,
                    weaponFrameName: string, bodyWidth: number, currentHealth: number, maxHealth: number,
                    explosionKey: string) {
            super(game, x, y, imageKey, bodyFrames, weaponFrameName, bodyWidth, currentHealth, maxHealth, explosionKey);

            this.initializeAdditionalComponents();
            this.tracksCollection = [];
            this.fireAllowed = true;
            this.body.collideWorldBounds = true;
        }

        destroy() {
            this.smokeEmitter.destroy();
            this.smokeEmitter = null;
            this.tracksCollection.forEach((trackSprite) => {
                if (trackSprite) {
                    trackSprite.destroy();
                    trackSprite = null;
                }
            });
            this.tracksCollection = null;
            super.destroy();
        }

        private initializeAdditionalComponents() {
            this.smokeEmitter = this.game.add.emitter();
            this.addChild(this.smokeEmitter);
            this.smokeEmitter.position.set(this.width * 0.5, this.height * 4);
            this.smokeEmitter.width = this.width;
            this.smokeEmitter.setXSpeed(-this.width * 0.25, this.width * 0.25);
            this.smokeEmitter.setYSpeed(this.width * 0.35, this.width * 2);
            this.smokeEmitter.makeParticles(this.game.cache.getBitmapData(SMOKE_BMD_KEY));
            this.smokeEmitter.particleAnchor.set(0.5);
            this.smokeEmitter.setAlpha(0.3, 0.7, 30);
        }

        emitSmoke(): void {
            if (!this.smokeEmitter.on) {
                this.smokeEmitter.start(false, 1000, 5, 100);
            }
        }

        createTracks(x: number, y: number, width: number, height: number,
                     angle: number, key: string): void {
            let track = this.game.add.sprite(x, y, key);
            track.anchor.set(0.5);
            track.width = width;
            track.height = height;
            track.angle = angle;
            track.alpha = 0;
            track.tint = 0x000000;
            this.tracksCollection.push(track);

            this.game.add.tween(track).to({alpha: 1}, 100, Phaser.Easing.Default, true, 0, 0, false);
            this.game.time.events.add(1000, () => {
                this.game.add.tween(track).to({alpha: 0}, 200, Phaser.Easing.Default, true, 0, 0, false)
                    .onComplete.add(() => {
                    track.destroy();
                })
            })
        }

        disableFireAbility(): void {
            this.fireAllowed = false;
        }

        resetFireAbility(): void {
            this.fireAllowed = true;
        }
    }
}
