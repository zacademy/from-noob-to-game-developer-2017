/**
 * Created by Academy on 4/12/2018.
 */

module Main {

    export abstract class Tank extends Phaser.Sprite {

        protected healthBarBMD: Phaser.BitmapData;
        protected healthBar: Phaser.Image;
        protected currentHealth: number;
        protected maximumHealth: number;
        protected explosion: Phaser.Sprite;
        protected fireAllowed: boolean;

        shellsCollection: Array<Shell>;
        turret: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number, imageKey: string, bodyFrames: Array<number>,
                    weaponFrameName: string, bodyWidth: number, currentHealth: number, maxHealth: number,
                    explosionKey: string) {
            super(game, x, y, imageKey);

            this.game.add.existing(this);
            this.shellsCollection = [];
            this.currentHealth = currentHealth;
            this.maximumHealth = maxHealth;
            this.initialize(imageKey, bodyFrames, weaponFrameName, bodyWidth, explosionKey);

            this.game.physics.enable(this, Phaser.Physics.ARCADE);
            this.body.bounce.setTo(1, 1);
            this.body.immovable = false;
        }

        destroy(): void {
            this.shellsCollection.forEach((shell) => {
                shell.destroy();
                shell = null;
            });
            this.shellsCollection = null;
            this.healthBarBMD.destroy();
            this.healthBarBMD = null;
            this.healthBar.destroy();
            this.healthBar = null;
            this.currentHealth = null;
            this.maximumHealth = null;
            this.explosion.destroy();
            this.explosion = null;
            this.turret.destroy();
            this.turret = null;
            this.fireAllowed = null;

            super.destroy();
        }

        update() {
            this.shellsCollection.forEach((shell) => {
                shell.update();
            });

            this.healthBar.x = this.x;
            this.healthBar.y = this.y - this.height;

            this.game.world.bringToTop(this);
            this.game.world.bringToTop(this.turret);
            this.game.world.bringToTop(this.healthBar);
            this.game.world.bringToTop(this.explosion);
        }

        private initialize(imageKey: string, bodyFrames: Array<number>, weaponFrameName: string,
                           bodyWidth: number, explosionKey: string) {
            this.anchor.set(0.5);
            this.width = bodyWidth;
            this.scale.y = this.scale.x;
            this.animations.add(MOVE_ANIMATION_KEY, bodyFrames, 15, true);

            this.turret = this.game.add.sprite(this.x, this.y, imageKey, weaponFrameName);
            this.turret.position = this.position;
            this.turret.anchor.set(0.5);
            this.turret.width = this.width * 0.5;
            this.turret.scale.y = this.turret.scale.x;

            this.healthBar = this.game.add.sprite(this.x, this.y);
            this.healthBar.anchor.set(0.5);
            this.drawHealth();

            this.explosion = this.game.add.sprite(0, 0, explosionKey);
            this.explosion.anchor.set(0.5);
            this.explosion.position = this.position;
            this.explosion.width = this.height * 2;
            this.explosion.height = this.explosion.width;
            this.explosion.alpha = 0;
            this.explosion.animations.add(EXPLOSION_ANIMATION_KEY, null, 20);
        }

        private drawHealthBMD(): void {
            if (this.healthBarBMD) {
                this.healthBarBMD.destroy();
            }

            this.healthBarBMD = this.game.make.bitmapData(this.width, this.height * 0.2, 'healthBMD');
            this.healthBarBMD.ctx.strokeStyle = "#000000";
            let lineWidth = this.healthBarBMD.height * 0.2;
            this.healthBarBMD.ctx.lineWidth = lineWidth;
            this.healthBarBMD.ctx.strokeRect(lineWidth * 0.5, lineWidth * 0.5,
                this.healthBarBMD.width - lineWidth, this.healthBarBMD.height - lineWidth);
            this.healthBarBMD.ctx.fillStyle = "#22800F";
            this.healthBarBMD.ctx.fillRect(lineWidth, lineWidth, (this.healthBarBMD.width - lineWidth * 2) *
                this.currentHealth / this.maximumHealth, this.healthBarBMD.height - lineWidth * 2);

        }

        private drawHealth(): void {
            this.drawHealthBMD();
            this.healthBar.loadTexture(this.healthBarBMD);
        }

        takeDamage(damageTaken: number): boolean {
            this.currentHealth -= damageTaken;
            this.drawHealth();

            if (this.currentHealth <= 0) {
                this.die();
            }

            return this.currentHealth <= 0;
        }

        die(): void {
            this.healthBar.alpha = 0;
            this.explosion.alpha = 0.9;
            this.explosion.play(EXPLOSION_ANIMATION_KEY);
            this.explosion.animations.currentAnim.onComplete.add(() => {
                this.destroy();
            })
        }

        startAnimation(): void {
            if (!this.animations.getAnimation(MOVE_ANIMATION_KEY).isPlaying) {
                this.animations.play(MOVE_ANIMATION_KEY);
            }
        }

        stopAnimation(): void {
            if (this.animations.getAnimation(MOVE_ANIMATION_KEY).isPlaying) {
                this.animations.stop(MOVE_ANIMATION_KEY, true);
            }
        }

        removeShell(shell: Shell): void {
            this.shellsCollection.splice(this.shellsCollection.indexOf(shell), 1);
        }

        fire(target: Phaser.Point, bulletVelocity: number, momentumVelocity: number): void {
            let shell: Shell = null;

            this.shellsCollection.forEach((someShell) => {
                if (someShell.checkAvailability()) {
                    shell = someShell;
                }
            });

            if (shell !== null) {
                shell.resetPosition(this.turret.position.clone(), this.turret.angle);
            }
            else {
                shell = new Shell(this.game, this.turret.x, this.turret.y, this.turret.width * 0.3,
                    this.turret.height * 0.3, this.turret.angle, SHELL_IMAGE_KEY, 0x000000);
                this.shellsCollection.push(shell);
                this.game.physics.enable(shell, Phaser.Physics.ARCADE);
            }

            let startingPosition = this.turret.position.clone();
            let directionNormalized = new Phaser.Point(target.x - startingPosition.x,
                target.y - startingPosition.y).normalize();
            let velocity = directionNormalized.multiply(bulletVelocity, bulletVelocity);
            let acceleratedVelocity = new Phaser.Point(this.body.velocity.x, this.body.velocity.y)
                .normalize().multiply(momentumVelocity, momentumVelocity).add(velocity.x, velocity.y);

            shell.launch(startingPosition, acceleratedVelocity, this.turret.angle);
        }

        canFire(): boolean {
            return this.fireAllowed;
        }
    }
}