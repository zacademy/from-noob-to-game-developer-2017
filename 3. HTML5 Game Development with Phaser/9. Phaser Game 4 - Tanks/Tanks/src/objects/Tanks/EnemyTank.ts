///<reference path="Tank.ts"/>

module Main {

    export class EnemyTank extends Tank {

        private speed: number;
        private movementDestination: Phaser.Point;
        private directionChangeTimer: Phaser.Timer;

        constructor(game: Phaser.Game, x: number, y: number, imageKey: string, bodyFrames: Array<number>,
                    weaponFrameName: string, bodyWidth: number, currentHealth: number, maxHealth: number,
                    explosionKey: string, minMovementSpeed: number, maxMovementSpeed: number, minDirChangeDelay: number,
                    maxDirChangeDelay: number) {
            super(game, x, y, imageKey, bodyFrames, weaponFrameName, bodyWidth, currentHealth, maxHealth, explosionKey);

            this.tint = 0xAC14AB;
            this.turret.tint = 0xCBBACB;

            this.speed = this.game.rnd.integerInRange(minMovementSpeed, maxMovementSpeed);
            this.generateDirection();
            this.directionChangeTimer = this.game.time.create();
            this.directionChangeTimer.loop(this.game.rnd.integerInRange(minDirChangeDelay, maxDirChangeDelay), () => {
                this.generateDirection();
            });
            this.directionChangeTimer.start();
            this.fireAllowed = false;
            this.game.time.events.add(ENEMY_FIRE_INITIAL_DELAY, () =>{
                this.fireAllowed = true;
            });

            this.body.mass = 10000;
        }

        destroy(): void {
            this.speed = null;
            this.movementDestination = null;
            this.directionChangeTimer.stop(true);
            this.directionChangeTimer.destroy();
            this.directionChangeTimer = null;

            super.destroy();
        }

        updateTargetedComponents(target: Phaser.Sprite): void {
            this.turret.rotation = Phaser.Point.angle(this.position, target.position) - Math.PI * 0.5;
            this.rotation = Phaser.Point.angle(this.position, this.movementDestination) + Math.PI * 0.5;

            let directionNormalized = new Phaser.Point(this.movementDestination.x - this.position.x,
                this.movementDestination.y - this.position.y).normalize();

            this.position.x += directionNormalized.x * this.speed;
            this.position.y += directionNormalized.y * this.speed;

            if (this.fireAllowed && this.game.physics.arcade.distanceBetween(this, target) < this.game.height) {
                this.fireAllowed = false;
                this.fire(new Phaser.Point(target.position.x, target.position.y), ENEMY_BULLET_VELOCITY,
                    ENEMY_BULLET_MOMENTUM_VELOCITY);

                this.game.time.events.add(ENEMY_FIRE_RELOAD_DELAY, () => {
                    this.fireAllowed = true;
                })
            }
        }

        private generateDirection(): void {
            let randomX = this.game.rnd.integerInRange(this.game.world.bounds.left + this.width,
                this.game.world.bounds.right - this.width);
            let randomY = this.game.rnd.integerInRange(this.game.world.bounds.top + this.height,
                this.game.world.bounds.bottom - this.height);

            this.movementDestination = new Phaser.Point(randomX, randomY);
        }
    }
}
