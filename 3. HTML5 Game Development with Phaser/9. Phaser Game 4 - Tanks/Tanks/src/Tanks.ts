/// <reference path = "../src/lib/phaser.d.ts"/>

module Main {
    class TanksGame extends Phaser.Game {
        constructor(width?: number, height?: number) {
            super(width, height, Phaser.AUTO, 'phaser-div', null, true, true);

            this.stage.disableVisibilityChange = true;

            this.state.add(PRELOAD_STATE_KEY, Preload, true);
            this.state.add(GAME_STATE_KEY, Game);
        }
    }

    window.onload = () => {
        Phaser.Device.whenReady(() =>{
            (<any>window).PhaserGlobal = {disableAudio: true};
            new TanksGame(1280, 720);
        })
    }
}