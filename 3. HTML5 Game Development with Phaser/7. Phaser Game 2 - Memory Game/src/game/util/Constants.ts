module Main {

    export const CARDS_BACK_KEY: string = 'cards-back';
    export const CARDS_FRONT_KEY: string = 'cards-front';

    export const MAIN_ANIMATION_KEY: string = 'main';
    export const DEFAULT_ANIMATION_FPS: number = 15;

    export const NUMBER_OF_ITEMS_TO_MATCH: number = 2;
    export const EMPTY_SPACE_X_MULTIPLIER: number = 0.3;

    export const LOCAL_STORAGE_RESIZE_KEY: string = 'localStorageResize';
}