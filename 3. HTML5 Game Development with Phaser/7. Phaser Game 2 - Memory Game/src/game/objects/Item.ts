module Main {
    export class Item {

        private game: Phaser.Game;
        private backImage: Phaser.Image;
        private frontImage: Phaser.Image;
        private clicker: Phaser.Image;
        private animation: Phaser.Sprite;
        private imageScaleX: number;
        animationKey: string;
        visualsGroup: Phaser.Group;
        id: number;
        showingFace: boolean;
        dispatcher: Phaser.Signal;
        isActive: boolean;

        constructor(game: Phaser.Game, id: number, animationKey: string, showingFace: boolean, isActive: boolean) {
            this.game = game;

            this.id = id;
            this.isActive = isActive;
            this.visualsGroup = this.game.add.group();
            this.showingFace = showingFace;
            this.animationKey = animationKey;
            this.dispatcher = new Phaser.Signal();
        }

        destroy(): void {
            if (this.backImage) {
                this.backImage.destroy();
                this.backImage = null;
                this.frontImage.destroy();
                this.frontImage = null;
                this.animation.animations.stop();
                this.animation.destroy();
                this.animation = null;
                this.clicker.destroy();
                this.clicker = null;
            }

            this.animationKey = null;

            this.id = null;
            this.showingFace = null;
            this.dispatcher.dispose();
            this.dispatcher = null;
        }

        handleInput(): void {
            if (!this.showingFace) {
                this.showFace();
            }

            this.dispatcher.dispatch();
        }

        addVisuals(x: number, y: number, imageSide: number, backImageKey: string, frontImageKey: string,
                   group: Phaser.Group): void {
            this.frontImage = this.addImage(x, y, frontImageKey, imageSide, group);
            this.backImage = this.addImage(x, y, backImageKey, imageSide, group);

            this.clicker = this.addImage(x, y, null, imageSide, group);
            this.clicker.inputEnabled = true;
            this.clicker.events.onInputDown.add(() => {
                this.handleInput();
            });

            this.imageScaleX = this.frontImage.scale.x;

            this.animation = this.game.add.sprite(x, y, this.animationKey);
            this.animation.anchor.set(0.5);
            this.animation.width = this.frontImage.width * 0.5;
            this.animation.scale.y = this.animation.scale.x;
            // let frames = Phaser.Animation.generateFrameNames('animation_flower_', 1, 15, '.png', 2);
            this.animation.animations.add(MAIN_ANIMATION_KEY, null, DEFAULT_ANIMATION_FPS, true);
            this.animation.alpha = 0;

            if (this.showingFace) {
                this.backImage.scale.x = 0;
                this.animation.alpha = 1;
                this.animation.animations.play(MAIN_ANIMATION_KEY);
            }
        }

        showFace(): void {
            this.game.input.enabled = false;
            this.showingFace = true;
            this.animation.animations.play(MAIN_ANIMATION_KEY);

            this.game.add.tween(this.backImage.scale).to({x: 0}, 150, Phaser.Easing.Default, true, 0, 0, false);
            this.game.add.tween(this.animation).to({alpha: 1}, 100, Phaser.Easing.Default, true, 150, 0, false);
            this.frontImage.scale.x = 0;
            this.game.add.tween(this.frontImage.scale).to({x: this.imageScaleX}, 150, Phaser.Easing.Default, true, 125, 0, false)
                .onComplete.add(() => {
                this.game.input.enabled = true;
                this.resetInput();
            });
        }

        showBack(): void {
            this.game.input.enabled = false;
            this.animation.animations.stop();
            this.game.add.tween(this.frontImage.scale).to({x: 0}, 150, Phaser.Easing.Default, true, 0, 0, false);
            this.game.add.tween(this.animation).to({alpha: 0}, 100, Phaser.Easing.Default, true, 0, 0, false);
            this.game.add.tween(this.backImage.scale).to({x: this.imageScaleX}, 150, Phaser.Easing.Default, true,
                0, 0, false).onComplete.add(() => {
                this.game.input.enabled = true;
                this.resetInput();
            })
        }

        makeInactive(): void {
            this.isActive = false;
            this.game.add.tween(this.animation).to({alpha: 0}, 100, Phaser.Easing.Default, true, 0, 0, false);
            this.clicker.inputEnabled = false;

            this.game.add.tween(this.visualsGroup).to({alpha: 0}, 150, Phaser.Easing.Default, true, 0, 0, false)
                .onComplete.add(() => {
                this.game.input.enabled = true;
            })
        }

        private resetInput(): void {
            this.clicker.inputEnabled = false;
            this.clicker.inputEnabled = true;
        }

        private addImage(x: number, y: number, key: string, sideSize: number, group: Phaser.Group): Phaser.Image {
            let image = this.game.add.image(x, y, key, null, group);
            image.anchor.set(0.5);
            image.width = sideSize;
            image.height = sideSize;

            return image;
        }
    }
}