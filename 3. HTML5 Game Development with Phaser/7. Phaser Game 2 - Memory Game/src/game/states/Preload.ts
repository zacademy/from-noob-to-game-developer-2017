///<reference path="../util/Constants.ts"/>

module Main {
    export class Preload extends Phaser.State {

        preload() {
            this.game.load.image(CARDS_BACK_KEY, 'assets/cards-back.png');
            this.game.load.image(CARDS_FRONT_KEY, 'assets/cards-front.png');

            this.game.load.atlas('animation_batkata', 'assets/animations/animation_batkata.png',
                'assets/animations/animation_batkata.json');
            this.game.load.atlas('animation_camera', 'assets/animations/animation_camera.png',
                'assets/animations/animation_camera.json');
            this.game.load.atlas('animation_eggs', 'assets/animations/animation_eggs.png',
                'assets/animations/animation_eggs.json');
            this.game.load.atlas('animation_fish', 'assets/animations/animation_fish.png',
                'assets/animations/animation_fish.json');
            this.game.load.atlas('animation_flower', 'assets/animations/animation_flower.png',
                'assets/animations/animation_flower.json');
            this.game.load.atlas('animation_football', 'assets/animations/animation_football.png',
                'assets/animations/animation_football.json');
            this.game.load.atlas('animation_gamepad', 'assets/animations/animation_gamepad.png',
                'assets/animations/animation_gamepad.json');
            this.game.load.atlas('animation_gift', 'assets/animations/animation_gift.png',
                'assets/animations/animation_gift.json');
            this.game.load.atlas('animation_golden_dice', 'assets/animations/animation_golden_dice.png',
                'assets/animations/animation_golden_dice.json');
            this.game.load.atlas('animation_guitar', 'assets/animations/animation_guitar.png',
                'assets/animations/animation_guitar.json');
            this.game.load.atlas('animation_laptop', 'assets/animations/animation_laptop.png',
                'assets/animations/animation_laptop.json');
            this.game.load.atlas('animation_map', 'assets/animations/animation_map.png',
                'assets/animations/animation_map.json');
            this.game.load.atlas('animation_mexican_flag', 'assets/animations/animation_mexican_flag.png',
                'assets/animations/animation_mexican_flag.json');
            this.game.load.atlas('animation_pizza', 'assets/animations/animation_pizza.png',
                'assets/animations/animation_pizza.json');
            this.game.load.atlas('animation_pumpkin', 'assets/animations/animation_pumpkin.png',
                'assets/animations/animation_pumpkin.json');
            this.game.load.atlas('animation_snow', 'assets/animations/animation_snow.png',
                'assets/animations/animation_snow.json');
            this.game.load.atlas('animation_star', 'assets/animations/animation_star.png',
                'assets/animations/animation_star.json');
            this.game.load.atlas('animation_tombstone', 'assets/animations/animation_tombstone.png',
                'assets/animations/animation_tombstone.json');
            this.game.load.atlas('animation_unicorn', 'assets/animations/animation_unicorn.png',
                'assets/animations/animation_unicorn.json');
            this.game.load.atlas('animation_water', 'assets/animations/animation_water.png',
                'assets/animations/animation_water.json');
        }

        create() {
            this.game.state.start('Game', true, false, true);
        }

        shutdown() {
            super.shutdown()
        }
    }
}