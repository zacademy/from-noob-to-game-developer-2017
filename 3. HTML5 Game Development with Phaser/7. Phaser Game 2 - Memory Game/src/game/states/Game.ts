module Main {
    export class Game extends Phaser.State {

        private items: Array<Item>;
        private wrongAttemptsCount: number;
        private gameWonText: Phaser.Text;
        private wrongText: Phaser.Text;
        private resizeLocked: boolean;
        private normalLoad: boolean;

        init(normalLoad: boolean) {
            this.normalLoad = normalLoad;
        }

        create() {
            this.game.stage.backgroundColor = "#832189";
            let importedItems: Array<{
                id: number,
                animationKey: string,
                isActive: boolean,
                showingFace: boolean
            }>;

            if (this.normalLoad) {
                this.wrongAttemptsCount = 0;
            }
            else {
                this.game.input.enabled = true;

                let resizeObject: {
                    wrongAttemptsCount: number,
                    items: Array<{
                        id: number,
                        animationKey: string,
                        isActive: boolean,
                        showingFace: boolean
                    }>
                } = JSON.parse(localStorage.getItem(LOCAL_STORAGE_RESIZE_KEY));
                localStorage.removeItem(LOCAL_STORAGE_RESIZE_KEY);

                this.wrongAttemptsCount = resizeObject.wrongAttemptsCount;
                importedItems = resizeObject.items;
            }

            this.gameWonText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.5, "Congrats! You've won!" +
                " Press me to restart!", {
                font: this.game.width * 0.05 + "px OpenSans-ExtraBold",
                fill: "#000000",
                align: "center",
                wordWrap: true,
                wordWrapWidth: this.game.width * 0.5
            });
            this.gameWonText.anchor.set(0.5);
            this.gameWonText.alpha = 0;

            this.wrongText = this.game.add.text(0, 0, 'Wrong: ' + this.wrongAttemptsCount.toString(), {
                font: this.game.height * 0.03 + "px OpenSans-ExtraBold",
                fill: "#00000"
            });

            this.wrongText.position.set(this.wrongText.width * 0.2, this.wrongText.height * 0.1);

            this.generateItems(importedItems);

            this.addResizeLogic();
        }

        render() {
            // this.game.debug.spriteBounds(this.items[0].backImage, "#000000", true)
        }

        // update() {
        //     this.game.world.bringToTop(this.items[0].visualsGroup)
        // }

        handleItemClicked(): void {
            this.checkRoundSuccess();
        }

        destroy() {
            this.items.forEach((item) => {
                item.destroy();
            });

            this.gameWonText.destroy();
            this.gameWonText = null;
        }

        shutdown() {
            this.destroy();
            super.shutdown();
        }

        generateItems(importedItems: Array<{
            id: number,
            animationKey: string,
            isActive: boolean,
            showingFace: boolean
        }>): void {
            this.items = [];

            if (!importedItems) {
                let atlases = this.game.cache.getKeys(Phaser.Cache.TEXTURE_ATLAS);
                let lookupString = 'animation';

                for (let i = 0; i < atlases.length; i++) {
                    if (atlases[i].indexOf(lookupString) === -1) {
                        atlases.splice(i, 1);
                        i--;
                    }
                }

                for (let i = 0; i < atlases.length; i++) {
                    for (let j = 0; j < 2; j++) {
                        this.items.push(new Item(this.game, i, atlases[i], false, true));
                    }
                }

                Phaser.ArrayUtils.shuffle(this.items);
            }
            else {
                importedItems.forEach((item) => {
                    this.items.push(new Item(this.game, item.id, item.animationKey, item.showingFace, item.isActive))
                })
            }

            let screenRatio = this.game.width / this.game.height;
            // width/height = cols/rows
            // cols / rows = screenratio
            // cols = screenratio * rows
            // rows x cols = number of items
            // rows * rows * screenratio = number of items
            // rows ^ 2 * screenratio = number of items
            // rows ^ 2 = number of items / screenratio
            // rows = sqrt (number of items / screenratio)

            // round for kind of best fit
            let rows = Math.round(Math.sqrt(this.items.length / screenRatio));
            //ceil to ensure that we always have enough space
            let cols = Math.ceil(this.items.length / rows);

            let totalEmptySpaceX = this.game.width * EMPTY_SPACE_X_MULTIPLIER;
            let itemSide = Math.ceil((this.game.width - totalEmptySpaceX) / cols);
            let emptySpaceXAdjusted = (this.game.width - (itemSide * cols)) / (cols + 1);
            let emptySpaceYAdjusted = (this.game.height - (itemSide * rows)) / (rows + 1);

            for (let i = 0; i < this.items.length; i++) {
                let x = emptySpaceXAdjusted + itemSide * 0.5 + (i % cols) * (itemSide + emptySpaceXAdjusted);
                let y = emptySpaceYAdjusted + itemSide * 0.5 + Math.floor((i / cols)) *
                    (itemSide + emptySpaceYAdjusted);

                if (this.items[i].isActive) {
                    this.items[i].addVisuals(x, y, itemSide, CARDS_BACK_KEY, CARDS_FRONT_KEY, this.items[i].visualsGroup);
                }

                this.items[i].dispatcher.add(() => {
                    this.handleItemClicked();
                })
            }
        }

        private checkRoundSuccess(): void {
            // debugger;
            let facingItems: Array<Item> = [];

            this.items.forEach((item) => {
                if (item.showingFace) {
                    facingItems.push(item);
                }
            });

            if (facingItems.length === NUMBER_OF_ITEMS_TO_MATCH) {
                let allItemsMatch = true;

                for (let i = 0; i < facingItems.length - 1; i++) {
                    for (let j = i + 1; j < facingItems.length; j++) {
                        if (facingItems[i].id !== facingItems[j].id) {
                            allItemsMatch = false;
                            break;
                        }
                    }

                    if (!allItemsMatch) {
                        break;
                    }
                }

                if (!allItemsMatch) {
                    this.wrongAttemptsCount++;
                    this.wrongText.setText('Wrong: ' + this.wrongAttemptsCount.toString());
                }

                for (let i = 0; i < facingItems.length; i++) {
                    facingItems[i].showingFace = false;

                    if (allItemsMatch) {
                        this.game.time.events.add(400, () => {
                            facingItems[i].makeInactive();
                        })
                    }
                    else {
                        this.game.time.events.add(400, () => {
                            facingItems[i].showBack();
                        })
                    }
                }

                if (this.checkGameWon()) {
                    // game won text
                    this.activeGameWonText(100);
                }
            }
        }

        private checkGameWon(): boolean {
            for (let i = 0; i < this.items.length; i++) {
                if (this.items[i].isActive) {
                    return false;
                }
            }

            return true;
        }

        private activeGameWonText(delay: number): void {
            this.game.add.tween(this.gameWonText).to({alpha: 1}, delay, Phaser.Easing.Elastic.In, true, 0, 0, false);
            this.gameWonText.inputEnabled = true;
            this.gameWonText.events.onInputDown.addOnce(() => {
                this.game.state.start(this.game.state.current, true, false, true);
            })
        }

        private addResizeLogic(): void {
            this.resizeLocked = false;

            window.onresize = () => {
                if (!this.resizeLocked) {
                    this.resizeLocked = true;

                    this.game.time.events.add(50, () => {
                        this.game.paused = true;
                        this.game.scale.setGameSize(window.innerWidth, window.innerHeight);
                        this.exportGame();
                        this.game.paused = false;
                        this.game.state.start(this.game.state.current, true, false, false);
                    })
                }
            }
        }

        private exportGame(): void {
            let itemsToExport: Array<Object> = [];

            this.items.forEach((item) => {
                itemsToExport.push({
                    id: item.id,
                    animationKey: item.animationKey,
                    isActive: item.isActive,
                    showingFace: item.showingFace
                })
            });

            localStorage.setItem(LOCAL_STORAGE_RESIZE_KEY, JSON.stringify({
                wrongAttemptsCount: this.wrongAttemptsCount,
                items: itemsToExport
            }))
        }
    }
}