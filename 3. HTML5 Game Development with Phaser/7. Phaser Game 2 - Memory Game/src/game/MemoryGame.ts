///<reference path="../lib/phaser.d.ts"/>
///<reference path="states/Game.ts"/>

module Main {
    export class MemoryGame extends Phaser.Game {

        constructor(width: number, height: number) {
            super(width, height, Phaser.AUTO, 'phaser-div', null, false, true);

            this.stage.disableVisibilityChange = true;

            this.state.add('Preload', Preload, true);
            this.state.add('Game', Game);
        }
    }

    window.onload = () => {
        Phaser.Device.whenReady(() => {
            (<any>window).PhaserGlobal = {disableAudio: true};

            // let width = 1280;
            // let height = 720;

            new MemoryGame(window.innerWidth, window.innerHeight);
        })
    };
}