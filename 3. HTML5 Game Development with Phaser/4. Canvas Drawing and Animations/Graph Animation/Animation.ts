///<reference path="Point.ts"/>
module Animation {
    const NUMBER_OF_POINTS = 50;
    const EDGE_DISTANCE = 200;

    export class Animation {
        ctx:any;
        canvasElement:any;
        points:Array<Point>;

        constructor() {
            this.points = [];
            this.canvasElement = document.getElementById("animation-canvas");
            this.canvasElement.width = window.innerWidth;
            this.canvasElement.height = window.innerHeight;

            this.ctx = this.canvasElement.getContext("2d");
            for (let i = 0; i < NUMBER_OF_POINTS; i++) {
                this.points.push(new Point(this.canvasElement.width, this.canvasElement.height));
            }

            setInterval(() => {
                this.animationUpdate(this.points, this.canvasElement, this.ctx);
            }, 1000 / 60);
        }

        animationUpdate(points: Array<Point>, canvasElement, ctx) {
            ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
            this.drawLines();
            points.forEach(point => {
                point.update(canvasElement.width, canvasElement.height);
                point.draw(ctx);
            });
        }

        drawLines() {
            for (let i = 0; i < this.points.length; i++) {
                for (let j = i + 1; j < this.points.length; j++) {
                    let point1 = this.points[i];
                    let point2 = this.points[j];
                    let distance = Math.sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
                    if (distance < EDGE_DISTANCE) {

                        this.ctx.globalAlpha = 0.6 - 0.6 * distance / 200;
                        this.ctx.strokeStyle = "#404040";
                        this.ctx.lineWidth = 2;
                        this.ctx.beginPath();
                        this.ctx.moveTo(point1.x, point1.y);
                        this.ctx.lineTo(point2.x, point2.y);
                        this.ctx.stroke();
                        this.ctx.globalAlpha = 1;
                    }
                }
            }
        }
    }
}