/// <reference path = "../lib/phaser.d.ts"/>

// CHANGE: to your game name
module GameName {
    // CHANGE: to your game name
    class GameName extends Phaser.Game {
        constructor(width?:number, height?:number) {
            // Creates the game
            super(width, height, Phaser.AUTO, 'phaser-div');
            this.state.add("Preload", Preload, false);
            this.state.add("Game", Game, false);

            this.state.start("Preload");
        }
    }

    window.onload = ()=> {
        // CHANGE: to your game name
        new GameName(1280, 720);
    }
}