/// <reference path = "../../lib/phaser.d.ts"/>

module GameName {
    export class Preload extends Phaser.State {
        background:Phaser.Image;

        preload() {
            //PRELOAD ALL YOUR GAME ASSETS HERE, e.g.
            this.game.load.image("background","src/assets/hackathon.jpg");
        }

        create() {
            // CREATE PRELOAD CONTENT HERE
            this.background = this.game.add.sprite(this.game.width/2, this.game.height/2, "background");
            this.background.anchor.set(0.5,0.5);
            this.background.scale.set(0.67,0.67);
            this.game.add.tween(this.background)
                .to({alpha:0.7},500, Phaser.Easing.Default,true,0,20,true);
        }
    }
}