﻿using System;
using System.IO;
using System.Threading;

class Hangman
{
    static void Main() 
    {
        Random rng = new Random();

        string[] dictionary = File.ReadAllLines("wordsEn.txt");
        string word = "";

        // We need a word larger than 4 symbols
        do
        {
            int randomWordIndex = rng.Next(9, dictionary.Length);
            word = dictionary[randomWordIndex];
        } while (word.Length <= 4);

        // Initialiazation
        int numberOfLives = 6;
        char[] currentGuess = new string('_', word.Length).ToCharArray();
        string allGuessedLetters = "";
        bool isGameOver = false;

        // Game Loop
        while(!isGameOver) {
            Console.Clear();

            // Drawing UI
            for (int i = 0; i < currentGuess.Length; i++) 
            {
                Console.WriteLine("{0} ", currentGuess[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Number of Lives: {0}", numberOfLives);
            Console.WriteLine();
            Console.WriteLine("Guessed Letters: {0}", allGuessedLetters);
            Console.Write("Please, input a letter: ");

            // Input
            string guessedLetter = Console.ReadLine();

            // Validating input
            bool isWrongInput = false;
            if (guessedLetter.Length > 1 || guessedLetter.Length == 0) 
            {
                Console.Clear();
                Console.WriteLine("Wrong Input! Please, input a single letter");
                Thread.Sleep(2500);
                isWrongInput = true;
            }
            else if (!Char.IsLetter(Convert.ToChar(guessedLetter))) 
            {
                Console.Clear();
                Console.WriteLine("Wrong Input! Please, input a single letter");
                Thread.Sleep(2500);
                isWrongInput = true;
            }

            // Main Logic
            bool isLetterCorrect = false;

            if (!isWrongInput) 
            {
                allGuessedLetters = allGuessedLetters + guessedLetter + ' ';

                for (int i = 0; i < currentGuess.Length; i++) 
                {
                    if (word[i] == Convert.ToChar(guessedLetter)) 
                    {
                        currentGuess[i] = Convert.ToChar(guessedLetter);
                        isLetterCorrect = true;
                    }
                }

                // Taking lives
                if (!isLetterCorrect) 
                {
                    numberOfLives--;
                }
            }

            // Game Over Condition
            if (numberOfLives == 0) 
            {
                Console.Clear();
                Console.WriteLine("Game over! The correct word was: {0}", word);
                isGameOver = true;
            }

            // Game Win Condition
            string hackString = new string(currentGuess);
            if (!hackString.Contains("_"))
            {
                Console.Clear();
                Console.WriteLine("You won! The correct word was {0}", word);
                isGameOver = true;
            }
        }
    }
}

