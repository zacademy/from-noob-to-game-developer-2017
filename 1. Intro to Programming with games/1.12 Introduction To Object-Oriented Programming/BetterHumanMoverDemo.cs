using System;
using System.Collections.Generic;
using System.Threading;

class HumanMoverDemo
{

    static Human human = new Human(2);

    struct BodyPart
    {
        public int x;
        public int y;
        public char symbol;

        public BodyPart(int x, int y, char symbol)
        {

            this.x = x;
            this.y = y;
            this.symbol = symbol;
            
        }
    }

    struct Human
    {
        public List<BodyPart> bodyParts;

        public Human(int a)
        {
            this.bodyParts = new List<BodyPart>();
            this.bodyParts.Add(new BodyPart(10, 10, '0'));
            this.bodyParts.Add(new BodyPart(10, 11, '|'));
            this.bodyParts.Add(new BodyPart(9, 11, '-'));
            this.bodyParts.Add(new BodyPart(11, 11, '-'));
            this.bodyParts.Add(new BodyPart(9, 12, '/'));
            this.bodyParts.Add(new BodyPart(11, 12, '\\'));
        }

        
    }

    enum Difficulty
    {
        Easy,
        Medium,
        Hard,
        Hardcore,
        Insane
    }

    enum DaysOfWeek
    {
        Monday,
        Tuesday,
        Wednesday
    }

    static void Main()
    {
        int a = 5;
        int b = a;
        b = 7;

        int[] c = { 1, 2, 3 };
        int[] d = c;
        d[0] = 5;


        bool isGameOver = false;
        Console.CursorVisible = false;

        Difficulty currentGameDifficulty = Difficulty.Insane;
        currentGameDifficulty = Difficulty.Hardcore;


        if (currentGameDifficulty == Difficulty.Insane)
        {
            //....
        }
    
        while (!isGameOver)
        {
            Console.Clear();
            DrawHuman();

            ConsoleKeyInfo key = Console.ReadKey();

            if (key.Key == ConsoleKey.LeftArrow)
            {
                human = Move(-1);
            }
            else if (key.Key == ConsoleKey.RightArrow)
            {
                Move(1);
            }
        }
    }


    static void DrawHuman()
    {
        foreach (var bodyPart in human.bodyParts)
        {
            Console.SetCursorPosition(bodyPart.x, bodyPart.y);
            Console.Write(bodyPart.symbol);
        }
    }

    static Human Move(int direction)
    {
        for (int i = 0; i < human.bodyParts.Count; i++)
        {
            human.bodyParts[i] = new BodyPart
            {
                x = human.bodyParts[i].x + direction,
                y = human.bodyParts[i].y,
                symbol = human.bodyParts[i].symbol
            };
        }

        return human;
    }
}

