﻿using System;
using System.Collections.Generic;
using System.Threading;

class SnakeGame
{
    public static bool isGameOver = false;
    public static Random rng = new Random();

    public class Coords
    {
        public int X;
        public int Y;

        public Coords(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public void Add(Coords newCoords)
        {
            this.X += newCoords.X;
            this.Y += newCoords.Y;
        }

        public bool isEqualTo(Coords newCoords)
        {
            return this.X == newCoords.X && this.Y == newCoords.Y;
        }
    }

    public class Snake
    {
        public List<Coords> snakeElements;

        public Snake()
        {
            this.snakeElements = new List<Coords>
            {
                new Coords(10,10),
                new Coords(10,9),
                new Coords(10,8),
                new Coords(10,7),
                new Coords(10,6),
            };
        }

        public void Draw(Coords direction)
        {
            // Case right
            char headSymbol = ' ';
            if (direction.X == 1)
            {
                headSymbol = '>';
            }
            if (direction.X == -1)
            {
                headSymbol = '<';
            }
            if (direction.Y == 1)
            {
                headSymbol = 'V';
            }
            if (direction.Y == -1)
            {
                headSymbol = '^';
            }
            Console.SetCursorPosition(this.snakeElements[0].X, this.snakeElements[0].Y);
            Console.Write(headSymbol);

            for (int i = 1; i < this.snakeElements.Count; i++)
            {
                Console.SetCursorPosition(this.snakeElements[i].X, this.snakeElements[i].Y);
                Console.Write("*");
            }
        }

        public void Delete()
        {
            for (int i = 0; i < this.snakeElements.Count; i++)
            {
                Console.SetCursorPosition(this.snakeElements[i].X, this.snakeElements[i].Y);
                Console.Write(" ");
            }
        }

        public void Update(Coords direction)
        {
            //Move Snake
            for (int i = this.snakeElements.Count - 1; i > 0; i--)
            {
                this.snakeElements[i].X = this.snakeElements[i - 1].X;
                this.snakeElements[i].Y = this.snakeElements[i - 1].Y;
            }
            this.snakeElements[0].Add(direction);

            // Teleport snake
            if (this.snakeElements[0].X == Console.BufferWidth)
            {
                this.snakeElements[0].X = 0;
            }
            else if (this.snakeElements[0].X == -1)
            {
                this.snakeElements[0].X = Console.BufferWidth - 1;
            }
            else if (this.snakeElements[0].Y == Console.BufferHeight)
            {
                this.snakeElements[0].Y = 0;
            }
            else if (this.snakeElements[0].Y == -1)
            {
                this.snakeElements[0].Y = Console.BufferHeight - 1;
            }

            // Game over check
            for (int i = 1; i < this.snakeElements.Count; i++)
            {
                if (this.snakeElements[0].isEqualTo(this.snakeElements[i]))
                {
                    isGameOver = true;
                }
            }
        }


    }

    public class Apple
    {
        public Coords Position;
        public bool isActive;
        // For proper Timer:
        // public int timeSinceLastSpawn;
        // public int spawnTimer;

        int timeSinceLastSpawn = Environment.TickCount;
        int respawnTime = 500;

        public Apple()
        {
            this.Position = new Coords(0, 0);
            this.isActive = false;
            // For proper Timer:
            // this.timeSinceLastSpawn = Environment.TickCount;
            // this.spawnTimer = 5000; -> 5 seconds delay between apple spawns
        }

        public void Draw()
        {
            if (this.isActive)
            {
                Console.SetCursorPosition(this.Position.X, this.Position.Y);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("@");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public void Update(Snake snake)
        {
            // For proper Timer:
            // if(this.timeSinceLastSpawn + this.spawnTimer<Environment.TickCount)
            // {
            //      this.timeSinceLastSpawn = Environment.TickCount; -> resets the timer
            //      this.Generate(snake); 
            // }
            if (Environment.TickCount % respawnTime == 0)
            {
                this.Generate(snake);
            }
        }

        public void Generate(Snake snake)
        {
            do
            {
                this.Position.X = rng.Next(0, Console.BufferWidth);
                this.Position.Y = rng.Next(0, Console.BufferHeight);
            } while (this.CollidesWithElements(snake));
        }

        public bool CollidesWithElements(Snake snake)
        {
            for (int i = 0; i < snake.snakeElements.Count; i++)
            {
                if (snake.snakeElements[i].isEqualTo(this.Position))
                {
                    return true;
                }
            }
            this.isActive = true;
            return false;
        }

        public void Delete()
        {
            if (this.isActive)
            {
                Console.SetCursorPosition(this.Position.X, this.Position.Y);
                Console.Write(" ");
            }
        }
    }

    public class Rock
    {
        public Coords Position;

        public Rock(Snake snake)
        {
            this.Generate(snake);
        }

        public void Draw()
        {
                Console.SetCursorPosition(this.Position.X, this.Position.Y);
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write("X");
                Console.ForegroundColor = ConsoleColor.White;
        }


        public void Generate(Snake snake)
        {
            this.Position = new Coords(0, 0);
            do
            {
                this.Position.X = rng.Next(0, Console.BufferWidth);
                this.Position.Y = rng.Next(0, Console.BufferHeight);
            } while (this.CollidesWithElements(snake));
        }

        public bool CollidesWithElements(Snake snake)
        {
            for (int i = 0; i < snake.snakeElements.Count; i++)
            {
                if (snake.snakeElements[i].isEqualTo(this.Position))
                {
                    return true;
                }
            }
            return false;
        }

    }
    static void Main()
    {
        Console.CursorVisible = false;
        Console.BufferWidth = Console.WindowWidth = 50;
        Console.BufferHeight = Console.WindowHeight = 25;
        double difficulty = 150;
        double changeDifficulty = 1;
        double worstDifficulty = 50;

        Apple apple = new Apple();
        Coords direction = new Coords(0, 1);
        Snake snake = new Snake();
        List<Rock> rocks = new List<Rock>();

        while (!isGameOver)
        {
            InputHandler(direction);
            snake.Update(direction);
            apple.Update(snake);
            SnakeAppleCollision(snake, apple, rocks);
            SnakeRocksCollision(snake, rocks);
            foreach (var rock in rocks)
            {
                rock.Draw();
            }

            apple.Draw();
            snake.Draw(direction);
            difficulty -= changeDifficulty;

            int sleepTime = (int)(Math.Max(Math.Round(difficulty), worstDifficulty));
            Thread.Sleep(sleepTime);
            apple.Delete();
            snake.Delete();
        }
    }

    static void InputHandler(Coords direction)
    {
        if (Console.KeyAvailable)
        {
            ConsoleKeyInfo userInput = Console.ReadKey();
            if (userInput.Key == ConsoleKey.RightArrow && direction.X != -1)
            {
                direction.X = 1;
                direction.Y = 0;
            }
            else if (userInput.Key == ConsoleKey.LeftArrow && direction.X != 1)
            {
                direction.X = -1;
                direction.Y = 0;
            }
            else if (userInput.Key == ConsoleKey.UpArrow && direction.Y != 1)
            {
                direction.X = 0;
                direction.Y = -1;
            }
            else if (userInput.Key == ConsoleKey.DownArrow && direction.Y != -1)
            {
                direction.X = 0;
                direction.Y = 1;
            }
        }
    }

    static void SnakeAppleCollision(Snake snake, Apple apple, List<Rock> rocks)
    {
        if (apple.isActive && snake.snakeElements[0].isEqualTo(apple.Position))
        {
            apple.isActive = false;
            snake.snakeElements.Add(new Coords(snake.snakeElements[snake.snakeElements.Count - 1].X,
                snake.snakeElements[snake.snakeElements.Count - 1].Y));
            rocks.Add(new Rock(snake));
            rocks.Add(new Rock(snake));
            rocks.Add(new Rock(snake));
        }
    }

    static void SnakeRocksCollision(Snake snake, List<Rock> rocks)
    {
        foreach (var rock in rocks)
        {
            if (rock.Position.isEqualTo(snake.snakeElements[0]))
            {
                isGameOver = true;
            }
        }
    }

}

