﻿namespace WoWClassTest
{
    using System;
    using System.Collections.Generic;
    using WoWCharacterModel.Enums;
    using WoWCharacterModel.Interfaces;
    using WoWCharacterModel.Model;

    public class EntyPoint
    {
        public static void Main()
        {
            Orc testOrc = new Orc("Stracimir-Ilarion", Race.Orc, Faction.Horde, 55, 
                7500, Class.DeathKnight, "green", 5.32);

            NPC testNpc = new NPC();

            Console.WriteLine(testOrc.Name);
            testOrc.ChangeName();
            Console.WriteLine(testOrc.Name);

            //Console.WriteLine(testNpc.Die());

            // Orc is-a Hero
            // Orc has-a Tusk
        }
    }
}
