﻿namespace WoWCharacterModel.Model
{
    public class NPC : Character
    {
        public NPC()
        {

        }

        public string Location { get; private set; }
        public bool IsAggressive { get; private set; }

        public string Respawn()
        {
            return "NPC respawned";
        }

        public override string Die()
        {
            return base.Die() + " overriden by class NPC";
        }

        public override string Move()
        {
            return "NPC moved";
        }
    }
}
