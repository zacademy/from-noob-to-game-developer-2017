﻿namespace WoWCharacterModel.Model
{
    public class Armor
    {
        public int Durability { get; private set; }
        public int Strength { get; private set; }
    }
}
