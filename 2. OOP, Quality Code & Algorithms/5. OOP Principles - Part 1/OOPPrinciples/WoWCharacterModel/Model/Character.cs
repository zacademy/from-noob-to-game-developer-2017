﻿namespace WoWCharacterModel.Model
{
    using WoWCharacterModel.Enums;
    using WoWCharacterModel.Interfaces;

    public abstract class Character: ICharacter
    { 
        public Character()
        {

        }

        public Character(string name)
        {
            this.Name = name;
        }

        public Character(string name, Race race, Faction faction, int level)
            :this(name)
        {
            this.Race = race;
            this.Faction = faction;
            this.Level = level;
            this.IsAlive = true;
        }

        public string Name { get; protected set; }
        public Race Race { get; private set; }
        public Faction Faction { get; private set; }
        public int Level { get; private set; }
        public bool IsAlive { get; private set; }

        public abstract string Move();

        public virtual string Die()
        {
            this.IsAlive = false;
            return "Character is dead";
        }
    }
}
