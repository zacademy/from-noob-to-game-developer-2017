﻿namespace WoWCharacterModel.Model
{
    using WoWCharacterModel.Enums;
    using WoWCharacterModel.Interfaces;

    public class Orc : Hero, IClass, ICharacter
    {
        public Orc(string name, Race race, Faction faction, int level, 
            int health, Class heroClass, string skinColor, double tuskLength)
            :base(name, race, faction, level, health, heroClass)
        {
            this.SkinColor = skinColor;
            this.TuskLength = tuskLength;
        }

        public string SkinColor { get; private set; }
        public double TuskLength { get; private set; }

        public string ChangeName()
        {
            this.Name = "Stefan";
            return this.Name;
        }

        public string Taunt()
        {
            return "Orc taunts";
        }

        public string BeProudMemberOfTheHorde()
        {
            return "Orc is in the best faction";
        }
    }
}
