﻿namespace WoWCharacterModel.Model
{
    using System.Collections.Generic;
    using WoWCharacterModel.Enums;
    using WoWCharacterModel.Interfaces;

    public abstract class Hero : Character, IClass, ICharacter
    {
        public Hero(string name, Race race, Faction faction, int level, int health, Class heroClass)
            :base("Pesho", race, faction, level)
        {
            this.Health = health;
            this.Class = heroClass;
        } 

        public int Health { get; private set; }

        public Class Class { get; private set; }
        public string Specialization { get; private set; }

        public string Attack()
        {
            return "Hero attacked";
        }

        public string LevelUp()
        {
            //this.Level++;
            return "Hero levels up!";
        }

        public string Equip(Armor armor)
        {
            return "Armor equipped with stats " + armor.Strength;
        }

        public override string Die()
        {
            //this.Level--;
            return base.Die() + " overriden by class Hero";
        }

        public override string Move()
        {
            return "Hero moved";
        }
    }
}
