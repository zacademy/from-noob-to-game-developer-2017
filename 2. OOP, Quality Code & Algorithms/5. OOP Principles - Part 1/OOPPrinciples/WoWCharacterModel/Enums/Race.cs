﻿namespace WoWCharacterModel.Enums
{
    public enum Race
    {
        Orc,
        Human,
        Tauren,
        Undead,
        Worgen
    }
}
