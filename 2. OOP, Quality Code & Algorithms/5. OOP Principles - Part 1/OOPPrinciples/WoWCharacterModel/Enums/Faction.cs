﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWCharacterModel.Enums
{
    public enum Faction
    {
        Horde,
        Alliance,
        Neutral
    }
}
