﻿namespace WoWCharacterModel.Interfaces
{
    using WoWCharacterModel.Enums;

    public interface ICharacter
    {
        string Name { get; }
        Race Race { get; }
        Faction Faction { get; }
        int Level { get; }
        bool IsAlive { get; }

        string Die();
        string Move();
    }
}
