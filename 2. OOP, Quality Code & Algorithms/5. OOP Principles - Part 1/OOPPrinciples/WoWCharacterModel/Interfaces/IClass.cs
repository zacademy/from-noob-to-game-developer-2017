﻿namespace WoWCharacterModel.Interfaces
{
    using WoWCharacterModel.Enums;

    public interface IClass
    {
        Class Class { get; }
        string Specialization { get; }
    }
}
