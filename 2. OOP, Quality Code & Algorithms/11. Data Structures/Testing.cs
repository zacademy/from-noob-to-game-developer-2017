﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresDemo
{
    class Testing
    {
        static void Main(string[] args)
        {
            //Testing Lists
            LinkedList<int> myList = new LinkedList<int>();

            myList.AddLast(1);
            myList.AddLast(2);
            myList.AddLast(3);
            myList.AddFirst(5);
            Console.WriteLine(myList);
            myList.Remove(2);
            Console.WriteLine(myList);
            myList.Remove(5);
            Console.WriteLine(myList);
            myList.Remove(3);
            Console.WriteLine(myList);
            myList.Remove(10);
            Console.WriteLine(myList);
            myList.Remove(1);
            Console.WriteLine(myList);
            Console.Clear();

            //Testing the tree
            Tree<int> myTree = new Tree<int>(1);
            myTree.Root.AddChildren(2, 3, 4);
            myTree.Root[0].AddChildren(5, 6);
            myTree.Root[2].AddChildren(7, 8, 9);
            Console.WriteLine(myTree);
            Console.Clear();

            //Testing Dictionary;
            Dictionary<string, int> myDictionary = new Dictionary<string, int>();
            myDictionary.Add("Pesho", 2); //myList.Add("Pesho")
            myDictionary["Gosho"] = 3; // myList[1] = "Gosho";

            int arrayLength = 12;
            var somethingToHash = "Pesho";
            Console.WriteLine(somethingToHash.GetHashCode()%12);
            DateTime somethingToHash2 = DateTime.Now;
            int veryBadHashCode = -123512421;
            Console.WriteLine(veryBadHashCode%12);




        }
    }
}
