﻿namespace DataStructuresDemo
{
    using System.Collections;
    using System.Text;

    public class LinkedList<T>:IEnumerable
    {
        private class Node
        {
            public T Value { get; set; }
            public Node Next { get; set; }

            public Node(T value)
            {
                this.Value = value;
            }
        }

        private Node first;
        private Node last;

        public LinkedList()
        {
            this.first = null;
            this.last = null;
        }

        public void AddLast(T value)
        {
            Node newNode = new Node(value);
            if (this.first == null)
            {
                this.first = newNode;
                this.last = this.first;
                return;
            }
            
            this.last.Next = newNode;
            this.last = newNode;
        }

        public void AddFirst(T value)
        {
            Node newNode = new Node(value);
            newNode.Next = this.first;
            this.first = newNode;

            if (this.last == null)
            {
                this.last = this.first;
            }
        }

        public void Remove(T value)
        {
            if (this.first == null)
            {
                return;
            }
            if (this.first.Value.Equals(value))
            {
                this.first = this.first.Next;
                return;
            }

            else
            {
                Node current = this.first.Next;
                Node previous = this.first;

                while (current != null)
                {
                    if (current.Value.Equals(value))
                    {
                        previous.Next = current.Next;
                        return;
                    }
                    previous = current;
                    current = current.Next; 
                }
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (Node node in this)
            {
                result.Append(node.Value + " ");
            }
            return result.ToString();
        }

        public IEnumerator GetEnumerator()
        {
            Node current = this.first;
            while (current != null)
            {
                yield return current;
                current = current.Next; 
            }
        }
    }
}
