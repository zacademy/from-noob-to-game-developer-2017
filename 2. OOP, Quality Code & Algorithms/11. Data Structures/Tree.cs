﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresDemo
{
    public class Tree<T>
    {
        public Vertex<T> Root { get; private set; }

        public Tree(T value)
        {
            this.Root = new Vertex<T>(value, 0);
        }


        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            Stack<Vertex<T>> searchStack = new Stack<Vertex<T>>();

            searchStack.Push(this.Root);
            while (searchStack.Top != null)
            {
                Vertex<T> currentVertex = searchStack.Pop();
                result.Append(new string(' ',currentVertex.Depth*4)+currentVertex.Value);
                result.AppendLine();
                currentVertex.Children.Reverse();
                foreach (var child in currentVertex.Children)
                {
                    searchStack.Push(child);
                }
                currentVertex.Children.Reverse();
            }

            return result.ToString();
            
        }
    }
}
