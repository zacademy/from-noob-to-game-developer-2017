﻿namespace DataStructuresDemo
{
    public class Stack<T>
    {
        public class Node
        {
            public T Value { get; set; }
            public Node Next { get; set; }

            public Node(T value)
            {
                this.Value = value;
            }
        }

        public Node Top;

        public Stack()
        {
            this.Top = null;
        }

        public T Pop()
        {
            if (this.Top == null)
            {
                return default(T);
            }
            T elementToReturn = this.Top.Value;
            this.Top = this.Top.Next;
            return elementToReturn;
        }

        public void Push(T value)
        {
            Node newNode = new Node(value);
            newNode.Next = this.Top;
            this.Top = newNode;
        }
    }
}
