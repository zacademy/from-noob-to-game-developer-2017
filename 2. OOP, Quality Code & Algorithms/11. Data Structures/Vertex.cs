﻿namespace DataStructuresDemo
{
    using System.Collections.Generic;
    using System.Linq;

    public class Vertex<T>
    {
        public T Value { get; private set; }
        public Vertex<T> Parent { get; private set; }
        public List<Vertex<T>> Children { get; private set; }
        public int Depth { get; private set; }

        public Vertex(T value, int depth)
        {
            this.Value = value;
            this.Depth = depth;
            this.Children = new List<Vertex<T>>();
        }

        public void AddChild(T value)
        {
            Vertex<T> child = new Vertex<T>(value, this.Depth + 1);
            child.Parent = this;
            this.Children.Add(child);
        }

        public void AddChildren(params T[] children)
        {
            foreach (var child in children)
            {
                this.AddChild(child);
            }
        }

        public void RemoveChild(T value)
        {
            this.Children.Remove
                (this.Children.FirstOrDefault
                (c=> { return c.Value.Equals(value); }));
        }

        public Vertex<T> this[int index]
        {
            get
            {
                return this.Children[index];
            }
        }
    }
}
