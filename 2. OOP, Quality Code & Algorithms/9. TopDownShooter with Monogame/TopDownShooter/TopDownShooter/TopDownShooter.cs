﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using TopDownShooter.GameObjects;
using TopDownShooter.Utilities;

namespace TopDownShooter
{
    public class TopDownShooter : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private const int GAME_WIDTH = 1300;
        private const int GAME_HEIGHT = 700;
        private const int SHOOT_DISTANCE = 30;

        private Texture2D background;
        private SpriteFont font;

        // Game objects we need:
        private Player player;
        private List<Zombie> zombies;
        private Cursor cursor;
        private List<BloodAnimation> bloodAnimations;

        private Random rng;
        private int zombiesKilledCounter;
        private double zombieSpawnTimer;
        private double zombieTimer;
        private Vector2 defaultZombieScale;


        public TopDownShooter()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = GAME_WIDTH;
            graphics.PreferredBackBufferHeight = GAME_HEIGHT;
            graphics.ApplyChanges();

            this.rng = new Random();
            this.cursor = new Cursor(Content);
            this.player = new Player(Content, GAME_WIDTH, GAME_HEIGHT, 200, 10, 8, new Vector2(0.45f, 0.45f));
            this.player.shootSignal += ShootZombies;
            this.zombieTimer = 0;
            this.zombieSpawnTimer = 1000;
            this.defaultZombieScale = new Vector2(0.45f, 0.45f);
            this.zombies = new List<Zombie>();
            this.zombiesKilledCounter = 0;

            this.bloodAnimations = new List<BloodAnimation>();

            base.Initialize();
        }


        protected override void LoadContent()
        {
            this.background = Content.Load<Texture2D>("Background");
            this.font = Content.Load<SpriteFont>("Font");
            spriteBatch = new SpriteBatch(GraphicsDevice);

        }


        protected override void UnloadContent()
        {
        }


        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            this.SpawnZombies(gameTime);
            this.cursor.Update();
            this.player.Update(gameTime, GAME_WIDTH, GAME_HEIGHT);
            this.bloodAnimations.RemoveAll((b) => { return b.IsAlive == false; });
            for (int i = 0; i < this.zombies.Count; i++)
            {
                if (!this.zombies[i].IsAlive)
                {
                    this.zombiesKilledCounter++;
                    this.zombies.RemoveAt(i);
                    i--;
                }
            }
            foreach (var zombie in this.zombies)
            {
                zombie.Update(gameTime, this.player.Position);
            }
            foreach (var blood in this.bloodAnimations)
            {
                blood.Update(gameTime);
            }
            base.Update(gameTime);
        }

        private void SpawnZombies(GameTime gameTime)
        {
            this.zombieTimer += gameTime.ElapsedGameTime.Milliseconds;

            if (this.zombieTimer >= this.zombieSpawnTimer)
            {
                this.zombieTimer = 0;
                this.zombieSpawnTimer = rng.Next(700, 2000);
                this.zombies.Add(new Zombie(Content, GAME_WIDTH, GAME_HEIGHT, 100000, 100, 0, this.defaultZombieScale, this.rng));
                this.zombies[this.zombies.Count - 1].zombieAttack += this.ZombieAttack;
            }
        }

        private void ZombieAttack()
        {
            this.player.TakeDamage(this.zombies[0].Damage);
        }

        private void ShootZombies()
        {
            if (this.zombies.Count > 0)
            {
                var direction = new Vector2(this.player.Position.X - this.cursor.Position.X,
                    this.player.Position.Y - this.cursor.Position.Y);
                Zombie zombieToShoot = null;
                double minDistance = 10000;

                foreach (var zombie in this.zombies)
                {
                    double currentDistance = OwnMath.GetDistanceBetweenPoints(zombie.Position.ToPoint(), player.Position.ToPoint());
                    float playerCursorAngle = OwnMath.CalculateAngleBetweenPoints(this.player.Position.ToPoint(), this.cursor.Position.ToPoint());
                    float playerZombieAngle = OwnMath.CalculateAngleBetweenPoints(this.player.Position.ToPoint(), zombie.Position.ToPoint());
                    float angle = Math.Abs(playerZombieAngle - playerCursorAngle);
                    var distance = currentDistance * Math.Sin(angle);
                    if (distance < minDistance && Math.Abs(angle)<Math.PI/4)
                    {
                        zombieToShoot = zombie;
                        minDistance = distance;
                    }
                }
                if (minDistance< SHOOT_DISTANCE)
                {
                    zombieToShoot.TakeDamage(this.player.Damage);
                    this.bloodAnimations.Add(new BloodAnimation(Content, zombieToShoot.Position));
                }

            }
        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            if (this.player.IsAlive)
            {
                spriteBatch.Draw(this.background, Vector2.Zero);
                spriteBatch.DrawString(this.font, "Health: " + this.player.Health, new Vector2(10, 10), Color.IndianRed);
                spriteBatch.DrawString(this.font, "KILLS: "+this.zombiesKilledCounter, new Vector2(180, 10), Color.IndianRed);
                this.player.Draw(spriteBatch);
                foreach (var zombie in this.zombies)
                {
                    zombie.Draw(spriteBatch);
                }
                foreach (var blood in this.bloodAnimations)
                {
                    blood.Draw(spriteBatch);
                }
                this.cursor.Draw(spriteBatch);
            }
            else
            {
                GraphicsDevice.Clear(Color.Black);
                spriteBatch.DrawString(this.font, "GAME OVER", new Vector2(GAME_WIDTH / 2, GAME_HEIGHT / 2), Color.IndianRed);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
