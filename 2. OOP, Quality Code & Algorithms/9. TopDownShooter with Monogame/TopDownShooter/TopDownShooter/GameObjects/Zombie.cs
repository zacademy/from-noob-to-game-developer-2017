﻿namespace TopDownShooter.GameObjects
{
    using global::TopDownShooter.Utilities;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System;

    public delegate void AttackSignal();

    public class Zombie : Entity
    {
        private const string MOVE_ANIMATION_KEY = "zombieMoveAnimation";
        private const string ATTACK_ANIMATION_KEY = "zombieAttackAnimation";
        private const float DEFAULT_ACCELERATION = 0.6f;
        private const int SPAWN_BORDER_OFFSET = 100;
        private const int ATTACK_DISTANCE = 20;
        private const double ATTACK_ANIMATION_TIMER = 200;
        private const double KNOCKBACK_VELOCITY = -8;

        private double attackTimer;
        private float maxVelocity;
        private float acceleration;

        public event AttackSignal zombieAttack;

        public Zombie(ContentManager Content, int gameWidth, int gameHeight, double health, double damage, double velocity, Vector2 scale, Random rng)
            :base(Content, gameWidth, gameHeight, health, damage, velocity, scale)
        {
            
            this.currentAnimationKey = MOVE_ANIMATION_KEY;
            this.maxVelocity = (float)(rng.NextDouble() * 3 + 5.8);
            this.velocity = maxVelocity;
            this.acceleration = DEFAULT_ACCELERATION;
            this.SpawnZombie(gameWidth, gameHeight, rng);
        }

        public void Update(GameTime gameTime, Vector2 playerPosition)
        {
            this.CheckCollision(gameTime, playerPosition);
            float angleBetweenZombieAndPlayer = OwnMath.CalculateAngleBetweenPoints(this.position.ToPoint(), playerPosition.ToPoint());
            Vector2 direction = this.CalculateDirection(playerPosition);

            if (this.currentAnimationKey == MOVE_ANIMATION_KEY)
            {
                this.velocity += this.acceleration;
                this.position.X += (float)(direction.X * velocity);
                this.position.Y += (float)(direction.Y * velocity);
            }
            else if (this.currentAnimationKey == ATTACK_ANIMATION_KEY)
            {
                this.attackTimer += gameTime.ElapsedGameTime.Milliseconds;

                if (this.attackTimer >= ATTACK_ANIMATION_TIMER)
                {
                    var distanceFromPlayerToZombie = OwnMath.GetDistanceBetweenPoints(this.position.ToPoint(), playerPosition.ToPoint());
                    if (distanceFromPlayerToZombie <= ATTACK_DISTANCE)
                    {
                        // Attack
                        this.zombieAttack.Invoke();
                        this.velocity = 0;
                        this.attackTimer = 0;
                    }
                    else
                    {
                        this.currentAnimationKey = MOVE_ANIMATION_KEY;
                    }
                }
              
            }

            foreach (var pair in this.animations)
            {
                this.animations[pair.Key].Update(gameTime);
            }
            this.rotation = angleBetweenZombieAndPlayer;
            if (this.velocity>this.maxVelocity)
            {
                this.velocity = maxVelocity;
            }
        }

        private void CheckCollision(GameTime gameTime, Vector2 playerPosition)
        {
            if (this.currentAnimationKey == MOVE_ANIMATION_KEY)
            {
                float distance = OwnMath.GetDistanceBetweenPoints(this.position.ToPoint(), playerPosition.ToPoint());
                if (distance<=ATTACK_DISTANCE)
                {
                    this.currentAnimationKey = ATTACK_ANIMATION_KEY;
                    this.velocity = 0;
                    this.attackTimer = 0;
                }
            }
        }

        private Vector2 CalculateDirection(Vector2 playerPosition)
        {
            float diffX = playerPosition.X - this.position.X;
            float diffY = playerPosition.Y - this.position.Y;
            Vector2 direction = new Vector2(diffX, diffY);

            direction.Normalize();
            return direction;
        }

        private void SpawnZombie(int gameWidth, int gameHeight, Random rng)
        {
            int randomDirection = rng.Next(0, 4);
            //left
            if (randomDirection == 0)
            {
                this.position = new Vector2(-SPAWN_BORDER_OFFSET, rng.Next(0, gameHeight));
            }
            //up
            else if (randomDirection == 1)
            {
                this.position = new Vector2(rng.Next(0, gameWidth), -SPAWN_BORDER_OFFSET);
            }
            //right
            else if (randomDirection == 2)
            {
                this.position = new Vector2(gameWidth+SPAWN_BORDER_OFFSET, rng.Next(0,gameHeight));
            }
            //down
            else if (randomDirection == 3)
            {
                this.position = new Vector2(rng.Next(0, gameWidth), gameHeight+SPAWN_BORDER_OFFSET);
            }
        }

        public override void TakeDamage(double damage)
        {
            this.Health -= damage;
            this.velocity = KNOCKBACK_VELOCITY;
            if (this.Health <= 0)
            {
                this.IsAlive = false;
            }
        }

        protected override void CreateAnimations(ContentManager Content)
        {
            var moveAnimation = Content.Load<Texture2D>(MOVE_ANIMATION_KEY);
            var attackAnimation = Content.Load<Texture2D>(ATTACK_ANIMATION_KEY);

            this.animations.Add(MOVE_ANIMATION_KEY, new Animation(moveAnimation, 17,6,3,288,314));
            this.animations.Add(ATTACK_ANIMATION_KEY, new Animation(attackAnimation, 9, 3, 3, 294, 318));
        }
    }
}
