﻿namespace _2.AnonymousTypes
{
    using System;
    
    class AnonTypes
    {
        public static void TestMethod(dynamic tablet)
        {
            Console.WriteLine("Screen Size " + tablet.Model);
        }

        static void Main()
        {
            var tablet = new { ScreenSize = "9.7", Model = "4", Brand = "Apple iPad" };
            TestMethod(tablet);

            Console.WriteLine(tablet.ScreenSize + " " + tablet.Model);

            var ints = new[] { 1, 2, 3, 5 };

            var anonymousTablets = new[]
            {
                new { ScreenSize = "10.5", Model = "2", Brand = ""},
                new { ScreenSize = "9.7", Model = "4", Brand = "Lenovo iPad" },
                new { ScreenSize = "8", Model = "4", Brand = "Apple iPad" },
                new { ScreenSize = "9.7", Model = "4", Brand = "Apple iPad" }
            };
        }
    }
}
