﻿namespace _1.ExtensionMethods.Extensions
{
    using System;
    using System.Collections.Generic;

    public static class ListExtensions
    {
        public static void IncreaseWithNumber(this List<int> myList, int number)
        {
            for (int i = 0; i < myList.Count; i++)
            {
                myList[i] += number;
            }
        }
    }
}
