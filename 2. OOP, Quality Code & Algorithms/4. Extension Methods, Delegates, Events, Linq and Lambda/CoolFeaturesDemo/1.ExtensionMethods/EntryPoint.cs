﻿
namespace _1.ExtensionMethods
{
    using System;
    using _1.ExtensionMethods.Extensions;
    using System.Collections.Generic;

    class EntryPoint
    {
        static void Main(string[] args)
        {
            string text = "random text random text random text";

            int wordsCount = text.CountWords();

            Console.WriteLine(wordsCount);

            Console.WriteLine();

            List<int> testList = new List<int>() { 1, 5, 10, 87, 13 };
            testList.IncreaseWithNumber(5);

            for (int i = 0; i < testList.Count; i++)
            {
                Console.WriteLine(testList[i]);
            }
        }
    }
}
