﻿namespace _6.EventsDemo2
{
    using System;
    using System.Threading;

    public delegate void KeyHandler(object sender, ConsoleKeyInfo pressedKey);

    public class HandleKey
    {
        public event KeyHandler OnPressed;

        private void OnPressedKey(ConsoleKeyInfo key)
        {
            if (this.OnPressed != null)
            {
                OnPressed(this, key);
            }
        }

        public void InputKey()
        {
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine("I am called from the class HandleKey. I have nothing to do with the event");
            Console.WriteLine("Pressed key {0}", pressedKey.Key);

            this.OnPressedKey(pressedKey);
        }
    }

    class EventsDemo2
    {
        public static void LoadRealmList(object sender, ConsoleKeyInfo key)
        {
            Console.WriteLine(key.Key);
            Console.WriteLine("Loading realmlist...");
            Thread.Sleep(1000);
            Console.WriteLine("realmlist loaded!");
        }

        public static void LoadAudio(object sender, ConsoleKeyInfo key)
        {
            Console.WriteLine(key.Key);
            Console.WriteLine("Loading audio...");
            Thread.Sleep(1000);
            Console.WriteLine("audio loaded!");
        }

        public static void LoadCharacter(object sender, ConsoleKeyInfo key)
        {
            Console.WriteLine(key.Key);
            Console.WriteLine("Loading Character...");
            Thread.Sleep(1000);
            Console.WriteLine("Character loaded!");
        }

        static void Main()
        {
            HandleKey keyHandler = new HandleKey();
            keyHandler.InputKey();

            keyHandler.OnPressed += LoadCharacter;
            keyHandler.OnPressed += LoadRealmList;
            keyHandler.OnPressed += LoadAudio;
            keyHandler.OnPressed -= LoadRealmList;

            keyHandler.InputKey();

        }
    }
}
