﻿
namespace _5.EventsDemo1
{
    using System;

    class EventsDemo1
    {
        public static void TestMethod(object sender, EventArgs e)
        {
            Console.WriteLine("TestMethod raised by event Changed in ListWithEvents with params {0} {1}", sender, e);
        }

        static void Main()
        {
            ListWithEvents testList = new ListWithEvents();
            testList.Add(5);
            testList.Changed += TestMethod;
            testList.Add(3);
        }
    }
}
