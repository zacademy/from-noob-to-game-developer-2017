﻿namespace _5.EventsDemo1
{
    using System;
    using System.Collections.Generic;

    public delegate void ChangedEventHandler(object sender, EventArgs e);

    public class ListWithEvents
    {
        public event ChangedEventHandler Changed;

        private List<int> myList = new List<int>();

        public int this[int index]
        {
            get
            {
                return this.myList[index];
            }
        }

        public int Count
        {
            get
            {
                return this.myList.Count;
            }
        }

        public void Add(int value)
        {
            this.myList.Add(value);
            this.OnChanged();
        }

        private void OnChanged()
        {
            if (this.Changed != null)
            {
                this.Changed(this, EventArgs.Empty);)
            }
        }
    }
}
