﻿
namespace _3.Delegates
{
    using System;

    public delegate void SomeDelegate(string text, int number);

    class Delegates
    {
        public static void TestMethod(string name, int age)
        {
            Console.WriteLine("Hi, my name is {0} and I am {1} years old.", name, age);
        }

        public static void SecondTestMethod(string t, int n)
        {
            Console.WriteLine("This is the second method with params {0} {1}", t, n);
        }

        public static void ExecuteDelegate(SomeDelegate d)
        {
            d("Gosho", 40);
        }

        static void Main()
        {
           //1 TestMethod("Plamen", 22);
           //2 TestMethod("Plamen", 22);
           //3 TestMethod("Plamen", 22);
           //4 SecondTestMethod("Plamen", 22);

            //SomeDelegate d = new SomeDelegate(TestMethod);
            SomeDelegate d1 = TestMethod;
            d1 += TestMethod;
            d1 += TestMethod;
            d1 += SecondTestMethod;

            ExecuteDelegate(d1);
        }
    }
}
