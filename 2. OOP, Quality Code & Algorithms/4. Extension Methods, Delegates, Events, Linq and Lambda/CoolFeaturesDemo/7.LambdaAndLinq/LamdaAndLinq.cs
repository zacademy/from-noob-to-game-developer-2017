﻿namespace _7.LambdaAndLinq
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections;

    public enum Size
    {
        S,
        M,
        L
    }

    class Pizza
    {
        public string Name { get; set; }
        public Size Size { get; set; }
        public int Price { get; set; }
    }

    class LamdaAndLinq
    {
        public static void TestMethod(string name, int age)
        {
            Console.WriteLine("Hi, my name is {0} and I am {1} years old.", name, age);
        }

        static void Main()
        {
            //Action<string, int> testAction = (name, age) => Console.WriteLine("Lamda: {0} {1}", name, age);
            //Action<int> testAction2 = (x) => Console.WriteLine(x + 7);

            //testAction2(3);

            var pizzaFactory = new List<Pizza>
            {
                new Pizza {Name = "Pepperoni", Size = Size.S, Price = 10},
                new Pizza {Name = "Gosho", Size = Size.L, Price = 30},
                new Pizza {Name = "Stracimir", Size = Size.M, Price = 1000},
                new Pizza {Name = "Chetiri Sirena", Size = Size.L, Price = 3},
                new Pizza {Name = "Quattro Salami", Size = Size.S, Price = 7},
                new Pizza {Name = "Pargarita", Size = Size.M, Price = 10},
                new Pizza {Name = "Balkanska", Size = Size.L, Price = 13}
            };

            var list = new List<int> { 4, 1, 2, 5, 10, 17, 3 };

            var lessThanFive =
                from n in list
                where n < 5
                select n;

            var orderBy =
                from n in list
                orderby n
                select n;

            var pizzaPricesAbove15 =
                from p in pizzaFactory
                orderby p.Name
                where p.Price > 15
                select p.Name + " " + p.Price;

            var groupedPizzas =
                from p in pizzaFactory
                orderby p.Name
                group p by p.Size;

            //foreach (var group in groupedPizzas)
            //{
            //    Console.WriteLine(group.Key);
            //    foreach (var pizza in group)
            //    {
            //        Console.WriteLine(pizza.Name + " " + pizza.Size);
            //    }
            //}

            IEnumerable pizzaCombinations =
                from pizza1 in pizzaFactory
                from pizza2 in pizzaFactory
                where pizza1.Size != pizza2.Size
                select new { FirstPizzaName = pizza1.Name, SecondPizzaName = pizza2.Name };

            //foreach (var combination in pizzaCombinations)
            //{
            //    Console.WriteLine(combination.FirstPizzaName + " " + combination.SecondPizzaName);
            //}

            // LAMBDA
            var filteredPizzas = pizzaFactory
                .Where(p => p.Name.StartsWith("P"))
                .OrderByDescending(p => p.Price)
                .Select(p => new { Name = p.Name, Price = p.Price })
                .ToList();

            //foreach (var pizza in filteredPizzas)
            //{
            //    Console.WriteLine(pizza.Name + " " + pizza.Price);
            //}

            var text = "Hi, py name is Plamen. I am 22";
            var result = text
                .Split(' ')
                .Where((w) => w.ToLower().StartsWith("i") || w.ToLower().StartsWith("p"));

            var cheapPizzas = pizzaFactory
                .Where(p => p.Price < 10)
                .Select(p => p.Name + " " + p.Price)
                .ToList();

            var firstPizza = pizzaFactory
                .Where(p => p.Price > 10000)
                .FirstOrDefault();
            // Console.WriteLine(firstPizza.Name);

            var anyExample = pizzaFactory
                .All(p => p.Price > 2);

            // Console.WriteLine(anyExample);

            var reversedPizzas = pizzaFactory
                .Where(p => p.Price > 10)
                .Reverse()
                .Select(p => p.Name);

            double averagePizzaPrice = pizzaFactory
                .Average(p => p.Price);

            Console.WriteLine(averagePizzaPrice);

            // Print(reversedPizzas);
        }

        public static void Print(IEnumerable collection)
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }
    }
}
