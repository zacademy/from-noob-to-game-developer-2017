﻿
namespace _4.ActionsAndFunc
{
    using System;

    class ActionsAndFunc
    {
        public static void TestMethod(string name, int age)
        {
            Console.WriteLine("Hi, my name is {0} and I am {1} years old.", name, age);
        }

        public static void SecondTestMethod(string t, int n)
        {
            Console.WriteLine("This is the second method with params {0} {1}", t, n);
        }

        public static string FuncMethod(bool isSunny, string grade, double number)
        {
            Console.WriteLine("FuncMethod");
            return grade + "aa";
        }

        public static string SecondFuncMethod(bool b, string c, double d)
        {
            Console.WriteLine("SecondFuncMethod");
            return c + "bb";
        }

        static void Main()
        {
            //SomeDelegate d = new SomeDelegate(TestMethod);
            Action<string, int> d1 = TestMethod;
            d1 += TestMethod;
            d1 += TestMethod;
            d1 += SecondTestMethod;

            d1("Plamen", 22);
            d1("Plamen", 22);

            Console.WriteLine();
            Func<bool, string, double, string> testFunc = FuncMethod;
            testFunc += SecondFuncMethod;

            string result = "";
            result += testFunc(false, "F", 123);

            Console.WriteLine(result);


        }
    }
}
