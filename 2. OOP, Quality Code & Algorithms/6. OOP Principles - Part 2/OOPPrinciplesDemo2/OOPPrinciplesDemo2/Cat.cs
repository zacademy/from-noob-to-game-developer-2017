﻿namespace OOPPrinciplesDemo2
{
    using OOPPrinciplesDemo2.Interfaces;

    public class Cat: Animal, IAnimal
    {
        public const string SOUND = "MEOW!";

        public Cat(string name)
            : base(name)
        {
        }
    
        public void Meow()
        {
            // NE E OK
            System.Console.WriteLine(SOUND);
        }

        public override string Speak()
        {
            return base.Speak() + " Also I am a Cat.";
        }

        public override string Sleep()
        {
            return "Sleeping.";
        }
    }
}
