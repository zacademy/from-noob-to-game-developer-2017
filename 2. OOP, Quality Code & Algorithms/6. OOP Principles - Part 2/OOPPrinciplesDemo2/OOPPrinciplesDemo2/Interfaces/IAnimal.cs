﻿namespace OOPPrinciplesDemo2.Interfaces
{
    public interface IAnimal
    {
        string Name { get;}

        string Speak();
    }
}
