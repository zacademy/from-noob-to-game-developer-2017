﻿namespace OOPPrinciplesDemo2
{
    using OOPPrinciplesDemo2.Interfaces;

    public abstract class Animal : IAnimal
    {
        public Animal(string name)
        {
            this.Name = name;
        }

        public string Name { get; private set; }

        public virtual string Speak()
        {
            return string.Format("Hi my names is {0}.", Name);
        }

        public virtual string Eat()
        {
            return "Eating.";
        }

        public abstract string Sleep();
    }
}
