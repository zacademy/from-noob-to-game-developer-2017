﻿namespace OOPPrinciplesDemo2
{
    using OOPPrinciplesDemo2.Interfaces;
    using System;
    using System.Collections.Generic;

    public class EntryPoint
    {
        public static void Main()
        {
            Cat cat = new Cat("Stracimir");
            //Console.WriteLine(cat.Speak());

            Panda panda = new Panda("Shibu");
            //Console.WriteLine(panda.Speak());

            Animal testCat = new Cat("Ilarion");
            //Console.WriteLine(testCat.Eat());

            List<IAnimal> animals = new List<IAnimal>();
            animals.Add(cat);
            animals.Add(testCat);
            animals.Add(panda);

            AnimalsSpeak(animals);
        }

        public static void AnimalsSpeak(IEnumerable<IAnimal> animals)
        {
            foreach (var animal in animals)
            {
                // Don't do that
                if (animal is Cat)
                {
                    var animalCat = animal as Cat;
                    Console.WriteLine(animalCat.Sleep());
                }

                Console.WriteLine(animal.Speak());
            }
        }
    }
}
