﻿
namespace OOPPrinciplesDemo2
{
    using OOPPrinciplesDemo2.Interfaces;

    public class Panda: Animal, IAnimal
    {
        public const string SOUND = "BRAAAH!";

        public Panda(string name)
            : base(name)
        {
        }

        public string Brah()
        {
            return SOUND;
        }

        public override string Speak()
        {
            return base.Speak() + " Also I am Panda.";
        }

        public override string Sleep()
        {
            return "Panda is sleeping";
        }
    }
}
