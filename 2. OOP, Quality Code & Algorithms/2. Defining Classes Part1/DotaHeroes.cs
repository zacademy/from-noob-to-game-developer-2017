﻿using System;

namespace DefiningClassesDemo
{

    public enum HeroType
    {
        Mage,
        Carry,
        Support,
        Tank
    }

    class Creep
    {
        private const int MAX_CREEP_HEALTH = 300;
        private const string DEFAULT_CREEP_NAME = "PeshoTheCreep";
        private const int DEFAULT_CREEP_HEALTH = 280;
        private const int DEFAULT_CREEP_DAMAGE = 20;

        private string name;
        private int health;
        private int damage;

        public string Name { get; private set; }
        public int Health
        {
            get
            {
                return this.health;
            }
            set
            {
                if (value <= 0 || value > MAX_CREEP_HEALTH)
                {
                    throw new ArgumentOutOfRangeException("Creep health should be between 0 and 300");
                }
                this.health = value;
            }
        }

        public int Damage
        {
            get
            {
                return this.damage;
            }
            set
            {
                if(value < 0)
                {
                    throw new ArgumentOutOfRangeException("Damage cannot be negative");
                }
                this.damage = value;
            }
        }

        public Creep() 
            :this(DEFAULT_CREEP_NAME, DEFAULT_CREEP_HEALTH, DEFAULT_CREEP_DAMAGE)
        {

        }

        public Creep(int health) 
            :this(DEFAULT_CREEP_NAME, health, DEFAULT_CREEP_DAMAGE)
        {

        }

        public Creep(int health, int damage)
            :this(DEFAULT_CREEP_NAME, health, damage)
        {

        }

        public Creep(string name, int health, int damage)
        {
            this.Name = name;
            this.Health = health;
            this.Damage = damage;
        }
        
        public int DealDamage()
        {
            return this.Damage;
        }

        public void TakeDamage(int damage)
        {
            if(this.health - damage <= 0)
            {
                Console.WriteLine("Creep is dead.");
                this.health = 0;
            }
            else
            {
                this.health -= damage;
            }
        }

    }

    class Hero
    {
        //Constants
        private const byte MIN_NAME_LENGTH = 2;
        private const short MAX_HEALTH = 1000;
        private const short MAX_MANA = 1050;
        private const HeroType DEFAULT_HERO_TYPE = HeroType.Carry;
        private const short DEFAULT_MANA = 300;
        private const short DEFAULT_HEALTH = 500;
        private const string DEFAULT_NAME = "Anti Mage";

        //readonly field 
        private readonly static Random rng = new Random(); 

        //Fields
        private string id;
        private string name;
        private int health;
        private int mana;


        //Properties
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value.Length < MIN_NAME_LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Hero name should not be less than 2 symbols");
                }
                this.name = value;
            }
        }

        public int Health
        {
            get
            {
                return this.health;
            }
            set
            {
                if(value <= 0 || value > MAX_HEALTH)
                {
                    throw new ArgumentOutOfRangeException("Hero cannot be dead!");
                }

                this.health = value;
            }
        }

        public int Mana
        {
            get
            {
                return this.mana;
            }
            set
            {
                if(value <= 0 || value > MAX_MANA)
                {
                    throw new ArgumentOutOfRangeException("You don't have enough mana.. you are probably dead!");
                }
                this.mana = value;
            }
        }

        public HeroType Type { get; private set; } // automatic property

        //Constructors
        public Hero() 
            :this(DEFAULT_NAME)
        {

        }
        public Hero(string name)
            :this(name, DEFAULT_HEALTH)
        {

        }
        public Hero(string name, int health)
            :this(name, health, DEFAULT_MANA)
        {
       
        }
        public Hero(string name, int health, int mana)
            :this(name, health, mana, DEFAULT_HERO_TYPE)
        {

        }
        public Hero(string name, int health, int mana, HeroType type)
        {
            this.id = GenerateID();
            this.Name = name;
            this.Health = health;
            this.Mana = mana;
            this.Type = type;
        }


        //Methods
        private string GenerateID()
        {
            int number = Hero.rng.Next(1, 100000);
            string finalID = this.Name + "_" + number;
            return finalID;
        }

        public void SayHi()
        {
            Console.WriteLine("You have picked {0}, who is a {1} type of hero and has {2} health" +
                "and {3} mana",this.Name, this.Type, this.Health, this.Mana);
        }

        public void TakeDamage(int damage)
        {
            if(this.Health -damage <= 0)
            {
                Console.WriteLine("Stop feeding you Noob");
                this.Health = 0;
            }
            else
            {
                this.Health -= damage;
            }
        }

        public void CastSpell(int manaCost)
        {
            if(this.Mana - manaCost <= 0)
            {
                Console.WriteLine("Not enough mana");
            }
            else
            {
                this.Mana -= manaCost;
            }
        }

        public override string ToString()
        {
            return "Name: " + this.Name + ", Health: " + this.Health +
                ", Mana: " + this.Mana + ", Hero Type: " + this.Type;
        }

        class Spell
        {
            private string name;
            private int cost;

            public string Name { get; private set; }
            public int Cost { get; private set; }

            public Spell(string name, int Cost)
            {
                this.Name = name;
                this.Cost = Cost;
            }
        }

    }

    class EntryPoint
    {
        static void Main()
        {
            Creep creep = new Creep();
            Hero firstPick = new Hero();
            Hero secondPick = new Hero("Axe", 640, 260, HeroType.Tank);
            Console.WriteLine(secondPick.ToString());
            Console.WriteLine("-------");
            Console.WriteLine(firstPick.Name);
            secondPick.CastSpell(120);
            Console.WriteLine(firstPick.ToString());
            //firstPick.TakeDamage(700);
            firstPick.TakeDamage(creep.DealDamage());
            Console.WriteLine(firstPick.ToString());

            creep.TakeDamage(650);

        }
    }
}
