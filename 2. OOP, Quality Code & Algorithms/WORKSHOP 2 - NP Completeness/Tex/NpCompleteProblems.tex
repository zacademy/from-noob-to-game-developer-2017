\documentclass[9.5pt]{article}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
 \usepackage{amsthm}
 
\usepackage{hyperref}
\usepackage{geometry}
 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }
\newtheorem{theorem}{Theorem}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem*{remark}{Remark}

\title{\vspace{-1.5cm}NP-Completeness}
\author{Martin Antonov}
\date{15 February 2018}

\begin{document}
\maketitle

\section{NP-Complete Problems}
\subsection*{Numerical Problems}
\begin{itemize}

\item \textbf{KNAPSACK:} Given a collection $\{(w_i,v_i)\}_{i=1}^n$ and a bound $W$, all taking values $\mathbb{N}$, to find the corresponding $x_i \in \{0,1\}$ so as to maximize $\sum_{i=1}^n x_iv_i$, subject to $\sum_{i=1}^n x_iw_i\leq W$. That is, given the weights and values of a set of items, we want to put in our knapsack items of maximum total value, subject to the we weight we can carry.

\item \textbf{SUBSET-SUM: } Given a set of integers $S=\{a_1,\cdots,a_n\}$ and an integer $L$, find a subset $I\subseteq S$ of the integers, such that $\sum_{i\in I} a_i = L$. In other words find a subset of the integers, which has sum $L$. 
\end{itemize}
\subsection*{Packing Problems}
\begin{itemize}
\item \textbf{BIN-PACKING:} Given a collection of weights $\{w_1,\cdots,w_n\}$ of items, such that $0<w_i\leq 1$, find a packing of these items into a minimum number of unit-sized bins. 
\end{itemize}
\subsection*{Covering Problems}
\begin{itemize}
\item \textbf{SET-COVER:} We are given a ground set of elements $E=\{e_1,\cdots ,e_n\}$, some subsets $S_1, S_2, \cdots, S_m$ where each $S_i \subseteq E$ and a non-negative weight $w_i \geq 0$ for each subset $S_i$. The goal is to find a minimum-weight collection of subsets that covers all of $E$, that is , we wish to find an $I\subseteq \{1,\cdots,m\}$ that minimizes $\sum_{i\in I}w_i$ subject to $\bigcup_{i\in I} S_i = E$. The unweighted version of the problem, where $w_i = 1$ is also NP-Complete.  

\item \textbf{VERTEX-COVER:} Given an undirected graph $G=(V,E)$, the goal is to find a subset $V'$ of the vertices, such that every edge in $G$ has an endpoint in $V'$. 
\end{itemize}
\subsection*{Constraint Satisfaction Problems}
\begin{itemize}
\item \textbf{3-SAT:} We are given a boolean formula in CNF (conjunctive normal form) of $n$ Boolean
 variables $x_1, \cdots x_n$ and $m$ clauses $C_1, \cdots , C_m$ with no more than $3$
  variables per clause. The goal is to find an assignment of the variables (with values $0$ or $1$) so
   that the formula is satisfied. An example of such a formula is given below:
   
$$\phi = (x_1 \lor x_2 \lor x_3)\land (x_1 \lor \bar{x_2} \lor x_3) \land (\bar{x_1} \lor \bar{x_2} \lor  \bar{x_3}) \land (x_1 \lor \bar{x_3})$$
There are many variants of this problems, which will be briefly discussed in the seminar.
\end{itemize}
\subsection*{Graph Theory Problems}
\begin{itemize}
\item \textbf{3-COLOURING:} Given a graph $G=(V,E)$, we need to assign one of three colours to each vertex, so that no two connected vertices have the same colour.
\item \textbf{HAMILTON-CYCLE:} Given a graph $G=(V,E)$, is there a cycle which goes through each vertex precisely once.
\item \textbf{TSP:} In the Travelling Salesman Problem we are given a complete weighted graph $G=(V,E)$. The goal is to find a Hamilton cycle with smallest weight.

\item \textbf{CLIQUE:} Given a graph $G=(V,E)$ the goal is to find a \emph{clique} of maximum size. A clique is a subset of the vertices, so that any two vertices in the set are adjacent. 
\item \textbf{INDEPENDENT-SET: } Given a graph $G=(V,E)$ the goal is to find an \emph{independent set} of maximum size. An independent set is a subset of the vertices, so that there are no edges between any two vertices in the set.
\end{itemize}

\section{Definitions and Theorems}
\theoremstyle{definition}
\begin{definition}
A \emph{problem} is a binary relation on problem instances and a set of problem solutions. 
\end{definition}
For example, in a sorting problem, a problem instance would a be a list of integers and the corresponding solution would be a sorted version of the list. Another example is, given a directed and weighted graph $G$ and two vertices $u$ and $v$ (the problem) to find the shortest path between $u$ and $v$ (the solution). 
\begin{definition}
A \emph{decision} problem is a problem with solutions $Yes$ or $No$. 
\end{definition}
We can formulate the shortest path problem by introducing a new variable, say $k$ and ask whether there exists a path from $u$ to $v$ with length at most $k$. 

We will assume that we can encode each problem, using only zeroes and ones. In other words, we encode with an alphabet $\Sigma = \{0,1\}$.

\begin{definition}
A \emph{language} $L$ over alphabet $\Sigma$ is any set of finite strings made up from symbols from $\Sigma$. 
\end{definition}
\begin{definition}
\emph{Complexity class P} is the set of languages $L$, such that there is a polynomiral-time algorithm that decides membership of a string in $L$. 
\end{definition}
For example, if $L$ is the language SHORTEST-PATH, then $x\in L$ can be represented as $<G,u,v,k>$.
\begin{definition}
A language $L$ is verified by a \emph{verification algorithm} $A$ if for every string $x\in L$, there exists a string $y$ such that $A(x,y)=1$ and for $x\notin L$, no such $y$ exists.
\end{definition}
In the above example, $x=<G,u,v,k>$, $y=<P>$, where $P$ is a path.
\begin{definition}
	\emph{Complexity class NP} is the set of languages $L$ such that there exists a polynomial-time algorithm $A$ and a constant $c$, such that \emph{(a)} for all $x\in L$, there is a $y$ such that $|y|=\mathcal{O}(|x|^c)$, \emph{(b)} $A(x,y)=1$ and \emph{(c)} for all $x\notin L$ there does not exist $y$ such that $A(x,y)=1$.
\end{definition}
Clearly, P$\subseteq$NP.
\begin{definition}
A language $L_1$ is polynomial-time reducible to a language $L_2$, denoted a $L_1\leq _P L_2$, if there exists a polynomial-time computable function $f:\{\Sigma _1 \}\rightarrow \{\Sigma _2\}$ such that, for all $x\in \Sigma _1$, $x\in L_1$ if and only if $f(x) \in L_2$. 
\end{definition}

\begin{theorem}
For two languages $L_1$ and $L_2\in$ P, if $L_1 \leq _P L_2$, then $L_1 \in$ P.
\end{theorem}

\begin{definition}
	 A language $L$ is \emph{NP-Complete} if \emph{(a)} $L\in $ NP and \emph{(b)} $L' \leq _P L$ for every $L'\in$ NP.
\end{definition}
The second property in the definition, means that $L$ is NP-hard.
\begin{theorem}
 If any NP-Complete problem is polynomial-time solvable, then $P=NP$.
\end{theorem}
There are problems which are in NP, but have not been proven to be NP-Complete. 
\begin{theorem}
There exists an NP-Complete problem. 
\end{theorem}
\begin{theorem}
If $L$ is a language such that $L'\leq _P L$ for some NP-Complete problem $L'$, then $L$ is NP-hard.
\end{theorem}
Finally, here is the recipe for proving that a problem $L$ is NP-Complete:
\begin{enumerate}
\item Prove $L\in NP$ (easy);
\item Select a known NP-Complete language $L'$;
\item Describe an algorithm that computes a function $f$, mapping every instance $x$ of $L'$ to an instance $f(x)$ of $L$;
\item Prove that the function $f$ satisfies $x\in L'$ if and only if $f(x)\in L$, for all $x$;
\item Prove that $f$ runs in polynomial time;
\end{enumerate}

In the lecture we will show the following:
\begin{itemize}
	\item E3-SAT is NP-Complete (reduction from 3-SAT).
	\item INDEPENDENT-SET is NP-Complete (reduction from VERTEX-COVER).
	\item CLIQUE is NP-Complete (reduction from INDEPENDENT-SET).
	\item TSP is NP-Complete (reduction from HAMILTON-CYCLE).
\end{itemize}
\end{document}
