﻿namespace Generics
{
    using System;
    using System.Collections.Generic;



    class GenericsDemo
    {
        static void Main()
        {
            Calculator calc = new Calculator();
            Console.WriteLine(calc.AreEqual<string>("a","b"));
            List<int> someList = new List<int>();

            MyList<int> list1 = new MyList<int>();
            Console.WriteLine("List at the beginning: "+list1.ToString());
            list1.Add(2);
            list1.Add(5);
            list1.Add(6);
            list1.Add(3);
            list1.Add(-10);

            MyList<int> list2 = new MyList<int>();
            Console.WriteLine("List at the beginning: " + list1.ToString());
            list2.Add(-2);
            list2.Add(-5);
            list2.Add(-6);
            list2.Add(120);
            list2.Add(-100);

            Console.WriteLine("List: "+list1.ToString()+"  cap: "+list1.Capacity);
            list1[0] = -100;
            Console.WriteLine("List1: " + list1.ToString());
            Console.WriteLine("List2: " + list2.ToString());
            Console.WriteLine("Adding: "+(list1+list2));




        }

        public class Calculator
        {
            public bool AreEqual<T>(T a, T b)
            {
                return a.Equals(b);
            }
        }
    }
}
