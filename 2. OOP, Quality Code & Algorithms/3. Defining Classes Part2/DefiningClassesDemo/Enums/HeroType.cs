﻿namespace DefiningClassesDemo.Enums
{
    public enum HeroType
    {
        Mage,
        Carry,
        Support,
        Tank
    }
}
