﻿namespace DefiningClassesDemo.Allies
{
    using System;
    using DefiningClassesDemo.Enums;

    public class Hero
    {
        // Constants
        private const byte MIN_NAME_LENGTH = 2;
        private const short MAX_HEALTH = 1000;
        private const short MAX_MANA = 1050;
        private const HeroType DEFAULT_HERO_TYPE = HeroType.Carry;
        private const short DEFAULT_MANA = 300;
        private const short DEFAULT_HEALTH = 500;
        private const string DEFAULT_NAME = "Anti Mage";

        // readonly field 
        private static readonly Random Rng = new Random();

        // Fields
        private string id;
        private string name;
        private int health;
        private int mana;

        // Constructors
        public Hero()
            : this(DEFAULT_NAME)
        {
        }

        public Hero(string name)
            : this(name, DEFAULT_HEALTH)
        {
        }

        public Hero(string name, int health)
            : this(name, health, DEFAULT_MANA)
        {
        }

        public Hero(string name, int health, int mana)
            : this(name, health, mana, DEFAULT_HERO_TYPE)
        {
        }

        public Hero(string name, int health, int mana, HeroType type)
        {
            this.id = this.GenerateID();
            this.Name = name;
            this.Health = health;
            this.Mana = mana;
            this.Type = type;
        }

        // Properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (value.Length < MIN_NAME_LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Hero name should not be less than 2 symbols");
                }

                this.name = value;
            }
        }

        public int Health
        {
            get
            {
                return this.health;
            }

            set
            {
                if (value <= 0 || value > MAX_HEALTH)
                {
                    throw new ArgumentOutOfRangeException("Hero cannot be dead!");
                }

                this.health = value;
            }
        }

        public int Mana
        {
            get
            {
                return this.mana;
            }

            set
            {
                if (value <= 0 || value > MAX_MANA)
                {
                    throw new ArgumentOutOfRangeException("You don't have enough mana.. you are probably dead!");
                }

                this.mana = value;
            }
        }

        public HeroType Type { get; private set; } // automatic property

        // Methods
        private string GenerateID()
        {
            int number = Hero.Rng.Next(1, 100000);
            string finalID = this.Name + "_" + number;
            return finalID;
        }

        public string SayHi()
        {
            return $"You have picked {this.Name}, who is a {this.Type} type of hero and has {this.Health} health and {this.Mana} mana";
        }

        public string TakeDamage(int damage)
        {
            if (this.Health - damage <= 0)
            {
                this.Health = 0;
                return "Stop feeding you Noob";
            }
            else
            {
                this.Health -= damage;
                return "Hero has taken damage!";
            }
        }

        public string CastSpell(int manaCost)
        {
            if (this.Mana - manaCost <= 0)
            {
                return "Not enough mana";
            }

            else
            {
                this.Mana -= manaCost;
                return "Spell casted";
            }
        }

        public override string ToString()
        {
            return "Name: " + this.Name + ", Health: " + this.Health +
                ", Mana: " + this.Mana + ", Hero Type: " + this.Type;
        }

        DateTime
    }
}
