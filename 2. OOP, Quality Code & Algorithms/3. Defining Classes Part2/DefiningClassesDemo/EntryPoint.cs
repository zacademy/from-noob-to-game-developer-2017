﻿namespace DefiningClassesDemo
{
    using DefiningClassesDemo.Allies;
    using DefiningClassesDemo.Enemies;
    using DefiningClassesDemo.Enums;
    using System;

    public class EntryPoint
    {
        private static void Main()
        {
            Creep creep = new Creep();
            Hero firstPick = new Hero();
            Hero secondPick = new Hero("Axe", 640, 260, HeroType.Tank);
            Console.WriteLine(secondPick.ToString());
            Console.WriteLine("-------");
            Console.WriteLine(firstPick.Name);
            Console.WriteLine(secondPick.CastSpell(120));
            Console.WriteLine(firstPick.ToString());
            // firstPick.TakeDamage(700);
            Console.WriteLine(firstPick.TakeDamage(creep.DealDamage()));
            Console.WriteLine(firstPick.ToString());

            creep.TakeDamage(650);
        }
    }
}
