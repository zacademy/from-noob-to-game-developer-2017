﻿namespace DefiningClassesDemo.Spells
{
    public class Spell
    {
        public string Name { get; private set; }
        public int Cost { get; private set; }

        public Spell(string name, int cost)
        {
            this.Name = name;
            this.Cost = cost;
        }
    }
}
