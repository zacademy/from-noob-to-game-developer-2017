﻿namespace DefiningClassesDemo.Enemies
{
    using System;

    public class Creep
    {
        private const int MAX_CREEP_HEALTH = 300;
        private const string DEFAULT_CREEP_NAME = "PeshoTheCreep";
        private const int DEFAULT_CREEP_HEALTH = 280;
        private const int DEFAULT_CREEP_DAMAGE = 20;

        private int health;
        private int damage;

        public string Name { get; private set; }
        public int Health
        {
            get
            {
                return this.health;
            }
            set
            {
                if (value <= 0 || value > MAX_CREEP_HEALTH)
                {
                    throw new ArgumentOutOfRangeException("Creep health should be between 0 and 300");
                }
                this.health = value;
            }
        }

        public int Damage
        {
            get
            {
                return this.damage;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Damage cannot be negative");
                }
                this.damage = value;
            }
        }

        public Creep()
            : this(DEFAULT_CREEP_NAME, DEFAULT_CREEP_HEALTH, DEFAULT_CREEP_DAMAGE)
        {

        }

        public Creep(int health)
            : this(DEFAULT_CREEP_NAME, health, DEFAULT_CREEP_DAMAGE)
        {

        }

        public Creep(int health, int damage)
            : this(DEFAULT_CREEP_NAME, health, damage)
        {

        }

        public Creep(string name, int health, int damage)
        {
            this.Name = name;
            this.Health = health;
            this.Damage = damage;
        }

        public int DealDamage()
        {
            return this.Damage;
        }

        public string TakeDamage(int damage)
        {
            if (this.health - damage <= 0)
            {
                this.health = 0;
                return "Creep is dead.";

            }
            else
            {
                this.health -= damage;
                return "Creep has taken damage!";
            }
        }

    }
}
