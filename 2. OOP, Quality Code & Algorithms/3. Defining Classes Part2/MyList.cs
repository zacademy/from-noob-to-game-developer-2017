﻿namespace Generics
{
    using System;

    public class MyList<T>
    {
        public int Count { get; private set; }
        public int Capacity { get; private set; } //TODO: fix private set!
        private const int INITIAL_CAPACITY = 4; 

        private T[] items;

        public MyList()
        {
            this.Capacity = INITIAL_CAPACITY;
            this.Count = 0;
            this.items = new T[this.Capacity];
        }

        public void Add(T newItem)
        {

            if (this.Count == this.Capacity)
            {
                this.DoubleArraySize();
            }

            this.items[this.Count] = newItem;

            this.Count++;
        }

        private void DoubleArraySize()
        {
            this.Capacity = this.Capacity * 2;
            T[] newArray = new T[this.Capacity];

            for (int i = 0; i <this.Capacity/2; i++)
            {
                newArray[i] = this.items[i];
            }

            this.items = newArray;
        }

        public override string ToString()
        {
            // TODO: Fix this, so that it does not print the last elements
            return string.Join(",", this.items);
        }

        public T this[int index]
        {
            get
            {
                // TODO: check if index is less than count
                return this.items[index];
            }
            set
            {
                this.items[index] = value;
            }
        }

        public static MyList<T> operator +(MyList<T> list1, MyList<T> list2)
        {
            MyList<T> result = new MyList<T>();

            if (list1.Count != list2.Count)
            {
                throw new InvalidOperationException("You can only add lists with the same number" +
                    "of elements");
            }

            for (int i = 0; i < list1.Count; i++)
            {
                result.Add((dynamic)list1[i] + (dynamic)list2[i]);
            }

            return result;
        }
    }
}
